<?php 

    /*------------------------------------*\
        Custom Post Types
    \*------------------------------------*/

    function gt_register_posttype( $_id, $_singular, $_plural, $_taxonomies, $_icon, $_excludefromsearch )
    {
        register_post_type($_id,
            array(
            'rewrite'  => array( 'slug' => $_id, 'with_front' => false ),
            'labels' => array(
                'name' => __( $_plural, 'groundtruth'),
                'singular_name' => __( $_singular, 'groundtruth'),
                'add_new' => __("Add New $_singular", 'groundtruth'),
                'add_new_item' => __("Add New $_singular", 'groundtruth'),
                'edit' => __("Edit", 'groundtruth'),
                'edit_item' => __("Edit $_singular", 'groundtruth'),
                'new_item' => __("New $_singular", 'groundtruth'),
                'view' => __("View $_singular", 'groundtruth'),
                'view_item' => __("View $_singular", 'groundtruth'),
                'search_items' => __("Search $_plural", 'groundtruth'),
                'not_found' => __("No $_plural found", 'groundtruth'),
                'not_found_in_trash' => __("No $_plural found in Trash", 'groundtruth'),
            ),
            'public' => true,
            'hierarchical' => true,
            'has_archive' => true,
            'supports' => array(
                'title',
                'editor',
                'excerpt',
                'thumbnail'
            ),
            'can_export' => true,
            'taxonomies' => $_taxonomies,
            'menu_icon' => $_icon,
            'exclude_from_search' => $_excludefromsearch,
            'show_in_rest' => true
        ));
    }

    function init_register_post_types(){
        gt_register_posttype( 'slider', 'Slideshow', 'Slideshows', array(), 'dashicons-align-center', true );
        gt_register_posttype( 'insight', 'Insight', 'Insights', array('category','solution','insight_type','post_tag'), 'dashicons-align-left', false );
        gt_register_posttype( 'event', 'Event', 'Events', array('category','solution','event_type','post_tag'), 'dashicons-align-left', false );
        gt_register_posttype( 'contact_form', 'Contact Form', 'Contact Forms', array(), 'dashicons-align-left', true );
    }
?>