<?php

function get_post_counts( $_post_type, $_tax_query, $_meta_query, $_full_tax_query, $_selected_terms ){
	$counts = [];

	foreach( $_full_tax_query as $tax ) : if( isset( $tax['taxonomy'] ) ) : foreach( $tax['terms'] as $term ) :
			//loop thorugh each term and build a query for each.

			$selected = $_selected_terms && array_search($term,$_selected_terms) !== false;

			$_new_query = ['relation'=>'AND'];

			$current_index = null;

			$empty = false;

			foreach($_tax_query as $new_tax) : if( isset( $new_tax['taxonomy'] ) ):
				if( $new_tax['taxonomy'] == $tax['taxonomy'] ){
					$new_terms = [];

					if( $selected === true ){
						//if selected remove it
						$current_index = array_search( $term, $new_tax['terms'] );
						$new_terms = $new_tax['terms'];
						array_splice( $new_terms, $current_index, 1);

						//if it's empty now add everything
						if( count( $new_terms ) === 0 ){
							$empty = true;
							$new_terms = $tax['terms'];
						}
					} else {
						if( count( $new_tax['terms'] ) == count( $new_tax['term_objects'] ) ){
							//if nothing no terms in this column only include this one
							$new_terms = [$term];
						} else {
							//if some terms are already in this column add this one on.
							$new_terms = $new_tax['terms'];
							array_push( $new_terms, $term );
						}
					}

					array_push( $_new_query, [ 'field'=>'term_id','taxonomy'=>$new_tax['taxonomy'], 'terms'=>$new_terms ] );
				} else {
					array_push( $_new_query, $new_tax );
				}
			endif; endforeach;

			$args = array(
				'post_type' => $_post_type,
				'tax_query' => $_new_query,
				'meta_query' => $_meta_query,
				'post_status' => 'publish',
				'suppress_filters' 	=> true,
				'count' => true
			);

			$query = new WP_Query( $args );
			$count = $query->found_posts;

			$counts[$term] = ['empty'=>$empty, 'current_index'=>$current_index, 'count'=>intval($count), 'selected'=>$selected, 'query'=>$_new_query, 'tax'=>$tax['taxonomy'], 'term'=>$term, 'selected_terms'=>$_selected_terms];

	endforeach; endif; endforeach;

	return $counts;
}

function ajax_posts_init_first_page(){
	global $num_posts_per_page;

	$post_type 				= $_POST['post_types'];
	$tax_query 				= $_POST['tax_query'];
	$meta_query 			= $_POST['meta_query'];
	$full_tax_query 		= $_POST['full_tax_query'];
	$selected_terms 		= $_POST['selected_terms'];
	$page_num  				= $_POST['page_num'];
	$counts 				= get_post_counts($post_type, $tax_query, $meta_query, $full_tax_query, $selected_terms );

	$this_page_args = array(
		'posts_per_page' 	=> $num_posts_per_page*$page_num,
		'orderby'          	=> 'date',
		'order'            	=> 'DESC',
		'post_type'        	=> $post_type,
		'tax_query'			=> $tax_query,
		'meta_query'		=> $meta_query,
		'post_status'      	=> 'publish',
		'suppress_filters' 	=> true,
		'count'				=> true
	);

	$the_query 			= new WP_Query( $this_page_args );
	$num_pages  		= $the_query->max_num_pages;
	$last_page 			= $num_pages == $page_num;

	$result = array(
		'counts'=>$counts,
		'num_pages'=>$num_pages,
		'post_type'=>$post_type,
		'tax_query'=>$tax_query
	);

	die( json_encode( $result ) );
}

function ajax_posts_get_posts_by_post_type() {
	global $num_posts_per_page;

	$post_type 					= $_POST['post_types'];
	$tax_query 					= $_POST['tax_query'];
	$full_tax_query 			= $_POST['full_tax_query'];
	$selected_terms 			= $_POST['selected_terms'];
	$meta_query 				= $_POST['meta_query'];
	$page_num  					= $_POST['page_num'];
	$grid_columns  				= $_POST['grid_columns'];
	$image_aspect_ratio  		= $_POST['image_aspect_ratio'];
	$counts 					= get_post_counts($post_type, $tax_query, $meta_query, $full_tax_query, $selected_terms);

	$args = array(
		'posts_per_page' 	=> $num_posts_per_page,
		'orderby'          	=> 'date',
		'order'            	=> 'DESC',
		'post_type'        	=> $post_type,
		'tax_query'			=> $tax_query,
		'meta_query'		=> $meta_query,
		'post_status'      	=> 'publish',
		'suppress_filters' 	=> true
	);

	$the_query 			= new WP_Query( $args );
	$num_pages  		= $the_query->max_num_pages;
	$last_page 			= $num_pages == $page_num;

	$offset 			= $num_posts_per_page*($page_num-1);
	$args['offset']		= $offset;

	$post_objects 		= get_posts( $args );
	$posts 				= [];

	foreach( $post_objects as $post ) {
		setup_postdata($post);

		$permalink = get_the_permalink( $post );
        $terms = get_the_terms($post->ID, 'insight_type');
        $tax = $terms[0];
        $taxlink = get_term_link($tax);

		$post_data = array(
			"post_type"=>$post->post_type,
			"title"=>$post->post_title,
			"excerpt"=>get_the_excerpt($post->ID),
			"permalink"=>$permalink,
			"thumbnail"=>get_the_post_thumbnail_url( $post,'large' ),
			"date"=>get_the_date("F d, Y",$post->ID),
            "tax"=>$tax->name,
            "taxlink"=>$taxlink
		);

		if( get_field('external_link', $post->ID ) ){
			$permalink = get_field('external_link', $post->ID );
			$post_data['permalink'] = $permalink['url'];
		}

		if( get_field('event_title', $post->ID ) ){
			$post_data['title'] = get_field('event_title', $post->ID );
		}

		if( !empty( get_field('event_start_date', $post->ID ) ) ){
            $start_date = DateTime::createFromFormat('Ymd', get_field( 'event_start_date', $post->ID ) );
			$post_data['date'] = $start_date->format('F d, Y');

			if( !empty( get_field('event_end_date', $post->ID ) ) ){
				$end_date = DateTime::createFromFormat('Ymd', get_field( 'event_end_date', $post->ID ) );
				$post_data['date'] .= ( " - " . $end_date->format('F d, Y') );
			}
		}

		if( get_field('event_location', $post->ID) ){
			$post_data['location'] = get_field('event_location', $post->ID );
		}

		if( get_field('event_icon', $post->ID ) ){
			$icon = get_field( 'event_icon', $post->ID );
            $post_data['event_icon'] = $icon['url'];
		}

		array_push( $posts, $post_data );

		wp_reset_postdata();
	}

	$result = array(
		'counts'=>$counts,
		'posts'=>$posts,
		'grid_columns'=>$grid_columns,
		'tax_query'=>$tax_query,
		'page_num'=>$page_num,
		'last_page'=>$last_page
	);

	die( json_encode( $result ) );
}