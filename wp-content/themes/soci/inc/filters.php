<?php
	/*------------------------------------*\
        Filters
	\*------------------------------------*/
	
	function change_default_jquery($scripts){
		if(!is_admin()){
			$scripts->remove( 'jquery');
		}
	}
	
	function add_query_vars($public_query_vars) {
	    $public_query_vars[] = 'pn';
	    return $public_query_vars;
	}
	
	// Remove Admin bar
	function remove_admin_bar()
	{
	    return false;
	}

	// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
	function remove_thumbnail_dimensions( $html )
	{
	    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
	    return $html;
	}

	function custom_short_excerpt($excerpt){
		$limit = 140;
	
		if (strlen($excerpt) > $limit) {
			return substr($excerpt, 0, strpos($excerpt, ' ', $limit)) . '...';
		}
		
		return $excerpt . ( strlen($excerpt) > 0 ? '...' : '' );
	}

	function cc_mime_types($mimes) {
		$mimes['svg'] = 'image/svg+xml';
		return $mimes;
	  }

	// Remove the <div> surrounding the dynamic navigation to cleanup markup
	function remove_nav_div($args = '')
	{
	    $args['container'] = false;
	    return $args;
	}

	// Remove invalid rel attribute values in the categorylist
	function remove_category_rel_from_category_list($thelist)
	{
	    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
	}

	function new_excerpt_more($more) {
		return '...';
	}

	// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
	function add_slug_to_body_class($classes)
	{
	    global $post;
	    if (is_home()) {
	        $key = array_search('blog', $classes);
	        if ($key > -1) {
	            unset($classes[$key]);
	        }
	    } elseif (is_page()) {
	        $classes[] = sanitize_html_class($post->post_name);
	    } elseif (is_singular()) {
	        $classes[] = sanitize_html_class($post->post_name);
	    }

	    return $classes;
	}
?>