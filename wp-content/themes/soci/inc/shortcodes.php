<?php 

function tax_in_content($atts){
    global $post;
    $html  = '';
    $taxonomy = 'insight_type';
    $terms = get_the_terms( $post, $taxonomy );

    if ( !empty( $terms ) ) {
        foreach ($terms as $term) {
            $html .= '<a href="' . get_term_link( $term, $taxonomy ) . '">' . $term->name . '</a>';
        }
    }

    return $html;
}

//Displays custom button shortcode
function button_shortcode( $atts, $content = null ) {
    //set default attributes and values
    $values = shortcode_atts( array(
            'url'   	=> '#',
            'target'	=> '_self',
            'id'   	    => '', // add id for anchor link
            'align'     => 'left'
    ), $atts );

    // align button left | center | right : default is left
    $align = esc_attr($values['align']);
    $before = '';
    $after = '';
    switch ($align) {
        case 'left': $align = 'uk-align-left';
            break;
        case 'right': $align = 'uk-align-right';
            break;
        case 'center': $align = 'uk-align-center uk-display-inline-block';
                        $before = '<span class="uk-text-center uk-display-block uk-margin-top">';
                        $after = '</span>';
            break;
        default: '';
    }
    return $before . '<a href="'. esc_attr($values['url']) . esc_attr($values['id']) .'"  target="'. esc_attr($values['target']) .'" class="uk-button uk-button-primary uk-margin ' . $align . '">'. $content .'</a>' . $after;
}