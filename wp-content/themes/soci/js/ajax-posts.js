var full_tax_query, loadmore, _querystring, fetching_posts = false;

$( function() {

	if( typeof(tax_query) !== 'undefined' ){
		
		full_tax_query = JSON.parse( JSON.stringify( tax_query ) );

		loadmore = $( "#loadmore" );

		loadmore.click( function(){

			fetch_posts( true );
		});

		$( "#taxonomy-menu" ).find( "ul.uk-nav>li>a" ).click( function( e ){
			e.preventDefault();

			var tax_id = $(this).data('term-id'),
			active = $(this).attr('data-active') === 'true',
			index = selected_terms.indexOf( tax_id ),
			li = $(this).parent("li");

			if( active ){
				li.removeClass('uk-active');

				if( index >= 0 ){
					//remove
					selected_terms.splice(index,1);
					remove = true;
				} else {
					//add
					li.addClass('uk-active');
					selected_terms.push( tax_id );
				}

				update_tax_query();
			}
		});
	}
		
	if( typeof(post_types) !== 'undefined' ) 
		initfirstpage();

	if( typeof(selected_terms) !== 'undefined' ){
		for(var i = 0; i<selected_terms.length; i++){
			selected_terms[i] = parseInt(selected_terms[i]);
		}
		selected_terms.forEach(term => {
			let $el = $("#taxonomy-menu a[data-term-id='"+term+"']");
			$el.attr('data-active', true);
			let $li = $el.parent();

			if(!$li.hasClass("uk-active")) $li.addClass("uk-active")
		});
	}
});

function fetch_posts_complete(){

	console.log( "fetch_posts_complete" );

	window.history.replaceState({}, '', `${location.pathname}?q=${selected_terms.join("|")}&pn=${page_num}`);

	fetching_posts = false;
}

function fetch_posts( _next_page, _saved_data ){

	fetching_posts = true;

	if( _next_page ){ page_num++; } else { page_num = 1; }

	let _data = _saved_data ? _saved_data : {
		action : 'ajax_posts_get_posts_by_post_type',
		post_types : post_types,
		tax_query : tax_query, 
		meta_query : meta_query, 
		full_tax_query : full_tax_query,
		selected_terms : selected_terms,
		page_num : page_num,
		dataType : 'json'
	}

	jQuery.ajax({
		url : ajax_posts.ajax_url,
		type : 'post',
		data : _data,
		success : function( response ) {

			response = JSON.parse( response );
			
			console.log( "ajax get posts success!", response );

			if( response.page_num == 1 ){ $( "#posts-container" ).empty(); }

			if( response.posts && response.posts.length > 0 ){

				loadposts( response.posts );
			} else {

				fetching_posts = false;
			}

			if( response.last_page ){
				if( !$("section.resource-grid-section").hasClass( "last-page" ) )
					$("section.resource-grid-section").addClass( "last-page" );
			} else {
				$( "section.resource-grid-section" ).removeClass( "last-page" );
			}

			$.each(response.counts,function(i,v){
				$term = $("#tax-term-" + i).find(">a").attr("data-active", v.count !== 0);
				// $term.find(" span.count").html(v.count);
			});	

		}, 
		error : function( error ) {

			console.log( "Ajax get posts - something bad happened: ", error );
		}
	}).done( function(){

		console.log( "Ajax get posts complete." );
	});
}

function update_tax_query(){
	console.log(selected_terms);

	tax_query = {};

	for( let index = 0; index < Object.keys( full_tax_query ).length; index++ ) {

		if( full_tax_query[ index ] ){

			var new_tax = JSON.parse( JSON.stringify( full_tax_query[ index ] ) ),

			terms = [];

			full_tax_query[ index ][ 'terms' ].forEach( term_id => {

				if( selected_terms.indexOf( term_id  ) >= 0 ){

					terms.push( term_id );
				}
			});

			if(terms.length == 0){
				terms = full_tax_query[ index ][ 'terms' ];
			}

			new_tax[ 'terms' ] = terms;

			if( terms.length > 0 ) tax_query[ index ] = new_tax;
		}
	}

	tax_query[ "relation" ] = "AND";

	if( Object.keys(tax_query).length === 1 ){
		tax_query = JSON.parse( JSON.stringify( full_tax_query ) );
	}

	fetch_posts( false );
}

function initfirstpage(){

	console.log( "initfirstpage", post_types );

	jQuery.ajax({
		url : ajax_posts.ajax_url,
		type : 'post',
		data : {
			action : 'ajax_posts_init_first_page',
			post_types : post_types,
			tax_query : tax_query,
			meta_query : meta_query,
			full_tax_query : full_tax_query,
			selected_terms : selected_terms,
			page_num : page_num,
			dataType : 'json'
		},
		success : function( response ) {

			response = JSON.parse( response );

			console.log( "Ajax get num pages - success: ", response );

			if( response.num_pages > 1 ){
				$("section.resource-grid-section").removeClass( "last-page" );
			} else {
				if( !$("section.resource-grid-section").hasClass( "last-page" ) )
					$("section.resource-grid-section").addClass( "last-page" );
			}

			$.each(response.counts,function(i,v){
				$term = $("#tax-term-" + i).find(">a").attr("data-active", v.count !== 0);
				// $term.find(" span.count").html(v.count);
			});	
		}, 
		error : function (error) {

			console.log( "Ajax get num pages - something bad happened: ", error );
		}
	}).done(function(){

		console.log("Ajax get num posts complete.");
	});
}

function loadposts(_posts){

	var _pi = 0;

	function loadpost(){

		get_post_element( _posts[_pi], function( _el ){

			$( "#posts-container" ).append( _el );
			
			if( _pi == _posts.length-1 ){

				fetch_posts_complete();
			}else{

				_pi++; loadpost();
			}
		});
	}

	loadpost();
}

function get_post_element( _post_data, _callback ){

	var _el = null;

	$.ajax({
        url: ajax_posts.posts_directory + _post_data.post_type + ".html.js",
        cache: true,
        dataType: "text",
        success: function( data ) {

			source    = data;
			
			template  = Handlebars.compile( source );
			
            _el = template( _post_data );
        },
        error : function (error) {

			console.log( "get_post_element - something bad happened: ", error );
		}             
    }).done(function() {

		console.log( "get_post_element done" );
		
    	_callback( _el );
	}); 
}
