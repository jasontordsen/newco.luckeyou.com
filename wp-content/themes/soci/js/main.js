var sections = [], didscroll = false, didresize = true, laststeptime = 0, mousedidmove = null, currentframe = 0, resizeto = null, slideshows = [], current_video_id = null, video_modal, setScrollTop = false;

/*======== READY ==========
===================================*/
$(function() {
    // $("#main-nav").on("show", function(e){
    //     var _t = $(this);

    //     setTimeout( function(){
    //         $("#main-nav").trigger("hide");
    //     },3000);
    // });

    //set scrolltop 
    

    $('form').submit(function(e){

        submit_form_id = $(this).attr("id");
        
        if( submit_form_id && !recaptcha_token ){

            e.preventDefault();

            console.log("No recatpcha token. Execute!");

            grecaptcha.execute();
        }
    }); 

    $('a[data-video-id]').click(function(e){
        e.preventDefault();
        current_video_id = $(this).attr('data-video-id');
        UIkit.modal(video_modal).show();
    });

    $("a[href^=\\#]").click(function (e) {
        e.preventDefault();
        var dest = $(this).attr('href');
        console.log(dest);
        $('html,body').animate({ scrollTop: $(dest).offset().top - 120 }, 'slow');
    })

    video_modal = $('#modal-media-video');

    video_modal.on('show', function(){
        console.log("show", current_video_id);

        var url = '';

        if( Number(current_video_id) ){
            url = 'https://player.vimeo.com/video/' + current_video_id+ '?autoplay=1';
        } else {
            url = 'https://www.youtube.com/embed/' + current_video_id + '?rel=0&start=0&autoplay=1';
        }

        $(this).find("iframe").eq(0).attr('src',url);
    });

    video_modal.on('hide', function(){
        console.log("hide", current_video_id);

        current_video_id = null;

        $(this).find("iframe").eq(0).attr('src','');
    });

    if(typeof(notification) !== 'undefined')
    UIkit.notification( "<span uk-icon='bell' class='uk-text-primary uk-margin-small-right'></span><span class='uk-text-small'>" + notification.message + "</span>", {pos: 'top-left'});

    $('section.slideshow-section').each(function(){
        var slideshow = {};

        slideshow.$el = $(this);
        slideshow.items = $(this).find(".uk-slideshow-items>li");

        slideshows.push(slideshow);
    });

    $(window).on( "scroll", onScroll );
    $(window).on( "resize", onResize );

    didresize = true;
    didscroll = true;

    window.requestAnimationFrame(step);

    if( window.location.search.indexOf("tolasttop") != -1 && localStorage.getItem('scrollTop') ){

        $('html,body').animate({ scrollTop: localStorage.getItem('scrollTop') }, 'slow');
    }
});

function onDidResize(){

    $.each( slideshows, function(i,slideshow){ 

        var height = 0;
        $.each(slideshow.items, function(ii,item){
            height = Math.max( height, $(item).height() );
        });

        slideshow.$el.height(height);
    });
}

function onDidScroll(){

    if( typeof(page_num) !== 'undefined' )
        localStorage.setItem("scrollTop", $("html,body").scrollTop());
}

/*======== EVENT HANDLERS ==========
===================================*/

function onScroll( _e ){
	didscroll = true;
}

function onResize( _e ){
	clearTimeout(resizeto);
    resizeto = setTimeout(function(){ didresize = true; }, 500 );
}

/*======== ANIMATION LOOP ==========
===================================*/
function step( time ){
	if( Math.round(currentframe)%20 === 0 ){
		if(didresize){
			onDidResize();
			didscroll = true;
			didresize = false;
		}
	}

	if( Math.round(currentframe)%20 === 0 ){
		if(didscroll){
			onDidScroll();
			didscroll = false; 
		}
	}

	laststeptime = time;
	currentframe++;
	window.requestAnimationFrame(step);
}