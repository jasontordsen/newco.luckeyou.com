<!doctype html>

<html class="no-js">
	<head>
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" type="text/css">

		<!-- TrustBox script --> 
		<script type="text/javascript" src="//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js" async></script> 
		<!-- End TrustBox script -->

		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-WH3QTXG');</script>
		<!-- End Google Tag Manager -->

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-51062980-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-51062980-1');
		</script> -->

		<!-- Bizable -->
		<script type="text/javascript" src="//cdn.bizible.com/scripts/bizible.js" async=""></script>
		<!-- End Bizable -->

		<!-- Start VWO Smartcode (SPA) -->
		<script type='text/javascript'>
		var _vwo_code=(function(){
		var account_id=431629,
		settings_tolerance=2000,
		library_tolerance=2500,
		use_existing_jquery=false,
		is_spa=1,
		/* DO NOT EDIT BELOW THIS LINE */
		f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&f='+(+is_spa)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
		</script>
		<!-- End VWO Smartcode (SPA) -->

		<!-- 6 Sense -->
		<script>
		    window._6si = window._6si || [];
		    window._6si.push(['enableEventTracking', true]);
		    window._6si.push(['setToken', '03a63fa02144c9052e9ab3d5740c2fca']);
		    window._6si.push(['setEndpoint', 'b.6sc.co']);

		    (function() {
		      var gd = document.createElement('script');
		      gd.type = 'text/javascript';
		      gd.async = true;
		      gd.src = '//j.6sc.co/6si.min.js';
		      var s = document.getElementsByTagName('script')[0];
		      s.parentNode.insertBefore(gd, s);
		    })();
		 </script>
		 <!-- End 6 Sense -->

		<!-- Pardot -->
		<script type="text/javascript">piAId = '354171'; piCId = '28024'; piHostname = 'pi.pardot.com';
		(function() {
			function async_load(){
				var s = document.createElement('script'); s.type = 'text/javascript';
				s.src = ('https:' == document.location.protocol ? 'https://pi' : 'http://cdn') + '.pardot.com/pd.js';
				var c = document.getElementsByTagName('script')[0]; c.parentNode.insertBefore(s, c);
			}
			if(window.attachEvent) { window.attachEvent('onload', async_load); }
			else { window.addEventListener('load', async_load, false); }
		})();</script>
		<!-- End Pardot -->

		<meta charset="<?php bloginfo('charset'); ?>">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<?php wp_head(); ?>

		<?php if( is_front_page() && get_field("notification",'option') ) : $notification = get_field("notification",'option'); ?>

		<!-- NOTIFICATION -->
		<script type="text/javascript">
			var link_val = JSON.parse( '<?php echo json_encode( $notification['link'] ); ?>' );
			var notification = { message: "<?php echo $notification['message']; ?>...<a class='uk-text-primary' href='"+link_val.url+"'><b>READ MORE</b></a>", link: link_val }
		</script>
		<?php endif; ?>

		<!-- /recaptcha -->
		<script type="text/javascript">
			var recaptcha_form_id = null;
			let recaptcha_token = false;

			function validateRecaptchaToken( _token, _form_id ) {

				fetch('/rcver.php', {
					method: 'POST',
					body:JSON.stringify({'token':_token}),
					headers:{ 'Content-Type': 'application/json' }
				})
				.then( res => res.json() )
				.then( response => {

					if( response.success ){
						
						console.log( 'validateRecaptchaToken: Success:', response );

						//All good. Submit the thing.
						$( "#"+_form_id ).css("opacity",".5");
						$( "#"+_form_id + " button" ).attr( "disabled", true );

						$("#"+_form_id).submit();

						recaptcha_token = null;
						recaptcha_form_id = null;
					} else {

						//This user sucks. Do nothing.
						console.log( 'validateRecaptchaToken: Error:', response );

						resetForm();
					}
				})
				.catch( error => {

					//This form sucks. Do nothing.
					console.log( 'validateRecaptchaToken: Error:', response );

					resetForm();
				});
			}

			function resetForm( _form_id ){

				$("#"+_form_id)[0].reset();

				recaptcha_token = null;
				recaptcha_form_id = null;
			}

			function recaptchaOnSubmit(_token){
				console.log("recaptchaOnSubmit");
				console.log(_token);

				recaptcha_token = _token; //get token here

				if( recaptcha_token ){

					validateRecaptchaToken( recaptcha_token, submit_form_id );
				} else {

					alert("no recaptcha token");
				}
			}
		</script>
		<script src="https://www.google.com/recaptcha/api.js" async defer></script>
	</head>
	<body <?php body_class(); ?>>

		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WH3QTXG" 
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->

		<?php get_template_part("templates/navigation/mobile_navigation"); ?>

		<?php get_template_part("templates/navigation/main_navigation"); ?>
		
		<!-- wrapper -->
		<div id="main-content" class="container-fluid">