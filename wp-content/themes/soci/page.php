<?php get_header(); ?>
<?php get_template_part( "templates/page_subnav" ); ?>
<?php get_template_part( "templates/sections" ); ?>

<?php 
    $show_contact_form = true; 
    if( get_field( 'show_contact_form' ) === false ){ $show_contact_form = false; }
    if( $show_contact_form === true ) : if( get_field("page_form", 'option') ) : 
?>
    <section class="form-section">
        <div class="uk-background-primary">
            <?php  
                $post = get_field("page_form", 'option');
                setup_postdata( $post ); 
                get_template_part("templates/posts/contact_form");
                wp_reset_postdata();
            ?>
        </div>
    </section>
<?php endif; endif; ?>

<?php get_footer(); ?>