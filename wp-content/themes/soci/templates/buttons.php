
<?php if( get_sub_field('buttons') ) : 
    $button_alignment = 'uk-text-left';

    if( get_sub_field("button_alignment") && is_string( get_sub_field("button_alignment") ) ){
        $button_alignment = get_sub_field("button_alignment");
    }
?>
<div class="uk-margin-top <?php echo $button_alignment; ?>">
    <?php while( have_rows('buttons') ): the_row(); ?>
        <?php 
            $link = get_sub_field('link'); 
            $url = $link['url'];
            $url_parts = explode('/', $url);
        ?>
        <?php if( $url_parts[0] == '#modal-video' ) : ?>
            <a class="uk-button uk-button-primary" href="#" data-video-id="<?php echo $url_parts[1]; ?>"><?php echo $link['title']; ?> <span uk-icon='play'></span></a>
        <?php else : ?>
            <a class="uk-button uk-button-primary" href="<?php echo $url; ?>" ><?php echo $link['title']; ?></span></a>
        <?php endif; ?>
    <?php endwhile; ?>
</div>
<?php endif; ?>