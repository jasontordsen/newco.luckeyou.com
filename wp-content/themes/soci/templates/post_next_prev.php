<?php 
    $prev_post = get_previous_post(); 
    $next_post = get_next_post(); 
?>

<div class="uk-background-primary">
    <nav id="next-prev-nav" class="uk-navbar-container uk-light uk-navbar-transparent" uk-navbar>
        <?php if($prev_post) : ?>
        <div class="uk-navbar-left">
            <ul class="uk-navbar-nav">
            <li><a href="<?php echo esc_url( get_permalink( $prev_post->ID ) ); ?>"><span uk-icon="icon: chevron-left; ratio: .8"></span>Previous</a></li>
            </ul>
        </div>
        <?php endif; ?>
        <?php if($next_post) : ?>
        <div class="uk-navbar-right">
            <ul class="uk-navbar-nav">
            <li><a href="<?php echo esc_url( get_permalink( $next_post->ID ) ); ?>">Next<span uk-icon="icon: chevron-right; ratio: .8"></span></a></li>
            </ul>
        </div>
        <?php endif; ?>
    </nav>
</div>