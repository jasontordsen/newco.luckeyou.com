<?php 
    $show_in_mobile_menu = get_sub_field('show_in_mobile_menu');

    if( !isset($show_in_mobile_menu )){
        $show_in_mobile_menu = true; 
    }
    
    $r_index = get_row_index(); 

    if($show_in_mobile_menu === true): if( get_row_layout() == 'navigation_item_menu' ): 
?>

    <!-- MENU LAYOUT -->
    <?php while( have_rows('navigation_item_menu_fields') ):  the_row(); ?>
        <li class="uk-parent">
            <a href=""><h4 class="uk-margin-left"><b><?php the_sub_field('navigation_item_menu_label'); ?></b></h4></a>
            <?php if( have_rows( "navigation_item_menu_items") ) : ?>
                <ul class="uk-nav-sub jt-background-light">
                    <?php while( have_rows('navigation_item_menu_items') ): the_row(); ?>
                        <?php if(get_sub_field('navigation_item_menu_item_title')) : $link = get_sub_field('navigation_item_menu_item_link'); ?>
                        <?php     
                            $show_in_mobile = get_sub_field('show_in_mobile_menu');

                            if( !isset($show_in_mobile )){
                                $show_in_mobile = true; 
                            }

                            if($show_in_mobile === true):
                        ?>
                            <li>
                                <a class="uk-display-inline-block uk-text-primary" href="<?php echo $link['url']; ?>"><h5><b><?php the_sub_field('navigation_item_menu_item_title'); ?></b></h5></a>
                            </li>
                        <?php endif; ?>
                        <?php endif; endwhile; ?>
                </ul>
            <?php endif; ?>
        </li>

    <?php endwhile; ?>

<?php elseif( get_row_layout() == 'navigation_item_link' ): ?>

    <!-- LINK LAYOUT -->
    <li>
        <?php $link = get_sub_field('navigation_item_link_link'); ?>
        <a href="<?php echo $link['url']; ?>"><h4 class="uk-margin-left"><b><?php echo $link['title']; ?></b></h4></a>
    </li>
    
<?php endif; endif; ?>