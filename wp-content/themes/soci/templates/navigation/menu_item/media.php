<?php if( get_sub_field('navigation_item_menu_media_image') || get_sub_field('navigation_item_menu_media_svg') ) :?>
    <?php $image = get_sub_field('navigation_item_menu_media_image') ; ?>

    <div class="uk-width-1-3@m">
        <?php if( get_sub_field('navigation_item_menu_media_title') ) :?>
            <h4 class="uk-text-primary"><?php the_sub_field('navigation_item_menu_media_title'); ?></h4>
        <?php endif; ?>

        <?php if( get_sub_field("navigation_item_menu_media_svg") ) : ?>
            <?php the_sub_field("navigation_item_menu_media_svg"); ?>
        <?php endif; ?>

        <?php if( !empty($image) ):  ?>
            <?php $aspect_ratio = '40';
                if( get_sub_field('navigation_item_menu_media_aspect_ratio') ){
                    $aspect_ratio = get_sub_field('navigation_item_menu_media_aspect_ratio');
                }
            ?>
            <div class="<?php echo get_sub_field('navigation_item_menu_media_size'); ?> jt-ar-<?php echo $aspect_ratio; ?>" uk-img data-src="<?php echo $image['url']; ?>">
                <?php if( get_sub_field("navigation_item_menu_media_video") ): ?>
                    <a class="uk-position-center uk-button uk-button-primary" data-video-id="<?php echo get_sub_field('navigation_item_menu_media_video'); ?>" href="#">
                        <span uk-icon="play"></span>
                    </a>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>