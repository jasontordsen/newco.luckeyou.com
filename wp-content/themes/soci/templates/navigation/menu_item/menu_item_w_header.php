<?php 
    $link = get_sub_field('navigation_item_menu_link');
?>

<li class="uline-nav-btn">
    <a href=""><h6><b><?php the_sub_field('navigation_item_menu_label'); ?></b></h6></a>
    <div class="uk-navbar-dropdown uk-padding jt-background-light">
        <div class="uk-child-width-expand@m" uk-grid>

            <?php if( get_sub_field('navigation_item_menu_header') ) :?>
                <div>
                    <div class="jt-small uk-padding-small">
                        <?php the_sub_field('navigation_item_menu_header') ; ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php get_template_part("templates/navigation/menu_item/media"); ?>
        </div>

        <?php get_template_part("templates/navigation/menu_item/items"); ?>
    </div>
</li>