<?php 
    $item_columns = "expand";
                
    if( get_sub_field("navigation_item_menu_item_columns") ){
        $item_columns = get_sub_field("navigation_item_menu_item_columns");
    }

    if( have_rows( "navigation_item_menu_items") ) : 
?>

    <div id="navigation_item_menu_items" class="uk-child-width-<?php echo $item_columns; ?>@m" uk-grid>
        <?php while( have_rows('navigation_item_menu_items') ): the_row(); ?>

            <?php 
                $item_link = get_sub_field('navigation_item_menu_item_link'); 
                $item_icon = get_sub_field('navigation_item_menu_item_icon'); 
                $item_image = get_sub_field('navigation_item_menu_item_image'); 
            ?>

            <a class="uk-display-block" href="<?php echo $item_link ? $item_link['url'] : ""; ?>">
                <div class="uk-padding-small uk-padding-remove-top">
                    <!-- ICON -->
                    <?php if( !empty($item_icon) ) : ?>
                        <p><img height="30" src="<?php echo $item_icon['url']; ?>" /></p>
                    <?php endif; ?>

                    <!-- TITLE -->
                    <?php if( get_sub_field('navigation_item_menu_item_title') ) : ?>
                        <h5 class="uk-text-primary"><b><?php the_sub_field('navigation_item_menu_item_title'); ?></b>   </h5>
                    <?php endif; ?>

                    <!-- IMAGE -->
                    <?php if( !empty($item_image) ) : ?>
                        <?php $image_aspect_ratio = '5625';
                            if( get_sub_field('navigation_item_menu_item_aspect_ratio') ){
                                $image_aspect_ratio = get_sub_field('navigation_item_menu_item_aspect_ratio');
                            }
                        ?>
                        <div class="jt-ar-<?php echo $image_aspect_ratio; ?> uk-background-cover" uk-img data-src="<?php echo $item_image['url']; ?>"></div>
                    <?php endif; ?>

                    <!-- DESCRIPTION -->
                    <?php if( get_sub_field('navigation_item_menu_item_description') ) : ?>
                        <h6 class="uk-margin-small-top jt-small"><b><?php the_sub_field('navigation_item_menu_item_description'); ?></b></h6>
                    <?php endif; ?>

                    <!-- LINK -->
                    <?php if( !empty( $item_link ) ) : ?>
                        <span class="jt-small uk-text-primary jt-text-link" uk-icon="icon: chevron-right; ratio: .8"><b><?php echo $item_link['title']; ?></b></span>
                    <?php endif; ?>
                </div>
            </a>

        <?php endwhile; ?>
    </div>

<?php endif; ?>