
<?php $video = get_sub_field('full_video_video'); 
$video_size = '';
if( get_sub_field('video_size') && is_string( get_sub_field('video_size') )){
    $video_size = get_sub_field('video_size');
} ?>

<section style="background-color:<?php the_sub_field("background_color"); ?>" class="full-video-section uk-background-cover uk-padding-remove-bottom">
    <div class="section-heading" uk-scrollspy="target: > *; cls:uk-animation-fade; delay:100;">
        <?php if( get_sub_field('full_video_heading') || get_sub_field('buttons') ) : ?>
            <div class="uk-padding uk-container">
                <?php if( get_sub_field('full_video_heading') ) : ?>
                    <?php the_sub_field('full_video_heading'); ?>
                <?php endif; ?>
                <?php get_template_part( "templates/buttons" ); ?>
            </div>
        <?php endif; ?>
    </div>
    <div class="uk-container <?php echo $video_size ?>">
        <div class="jt-ar-5625">
            <div class="uk-position-absolute uk-width-1-1 uk-height-1-1">
                <?php if( is_numeric( $video ) ) : ?>
                    <iframe src="https://player.vimeo.com/video/<?php echo $video ?>" width="100%" height="100%" frameborder="0" uk-video></iframe>
                <?php else :  ?>
                    <iframe src="https://www.youtube.com/embed/<?php echo $video ?>?rel=0&start=0&autoplay=1" width="100%" height="100%" frameborder="0" allowfullscreen></iframe>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>