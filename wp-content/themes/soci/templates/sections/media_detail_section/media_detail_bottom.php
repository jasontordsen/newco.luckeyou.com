<?php
    $overlay = get_sub_field("media_detail_overlay_detail");
    $anchorId = get_sub_field("media_detail_anchor_id");
?>

<section id="<?php echo $anchorId ?>" style="background-color:<?php the_sub_field("background_color"); ?>" class="media-detail-section media-detail-bottom uk-card uk-grid-collapse uk-child-width-1-1@m" uk-grid>
    <div class='<?php echo $overlay ? 'uk-overlay uk-position-absolute uk-height-1-1 jt-z-1' : ''; ?>'>
        <?php get_template_part( "templates/sections/media_detail_section/detail" ); ?>
    </div>
    <div>
        <?php get_template_part( "templates/sections/media_detail_section/media" ); ?>
    </div>
</section>