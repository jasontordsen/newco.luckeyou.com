<?php 
    $media_opacity = get_sub_field('media_detail_media_opacity');
    $image_size = get_sub_field('media_detail_image_size');
    $image_position = get_sub_field('media_detail_image_position'); 
    $image_link = get_sub_field('media_detail_image_link'); 
    $bg_video = get_sub_field('media_detail_background_video');

    $background_video_width = 1280;
    $background_video_height = 720;

    if( get_sub_field('background_video_original_size') ){
        $background_size = get_sub_field('background_video_original_size');
        if( $background_size['width'] ) $background_video_width = $background_size['width'];
        if( $background_size['height'] ) $background_video_height = $background_size['height'];
    }
?>

<div uk-scrollspy="cls:uk-animation-fade; delay:400;" class="media-detail-media uk-cover-container" >
    <?php $image = get_sub_field('media_detail_image'); ?>
    <div class="media jt-ar-<?php echo get_sub_field('media_detail_media_aspect_ratio'); ?>" style="background-color:<?php the_sub_field('media_detail_media_background_color'); ?>;">
        <div style="opacity:<?php echo $media_opacity; ?>;" class="uk-position-absolute uk-width-1-1 uk-height-1-1">
            <div class="media-image uk-position-absolute uk-width-1-1 uk-height-1-1" uk-img data-src="<?php echo $image ? $image['url'] : ""; ?>" style="background-repeat:no-repeat; background-position:<?php echo $image_position; ?>; background-size:<?php echo $image_size; ?>;"></div>
            <?php if( $bg_video )  : ?>
                <div class="uk-cover-container media-background-video uk-position-absolute uk-width-1-1 uk-height-1-1" style="top:0; left:0;">
                    <?php if( is_numeric( $bg_video ) ) : ?>
                        <iframe width="<?php echo $background_video_width; ?>" height="<?php echo $background_video_height; ?>" uk-cover src="https://player.vimeo.com/video/<?php echo $bg_video; ?>?background=1" frameborder="0"></iframe>
                    <?php else : ?>
                        <iframe width="<?php echo $background_video_width; ?>" height="<?php echo $background_video_height; ?>" uk-cover src="https://www.youtube.com/embed/<?php echo $bg_video ?>?rel=0&showinfo=0&mute=1&autoplay=1&controls=0&start=0&loop=true" frameborder="0"></iframe>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
            <?php if( get_sub_field('media_detail_media_svg') )  : ?>
                <div class="media-svg uk-padding uk-position-absolute uk-width-1-1 uk-height-1-1" style="top:0; left:0;" >
                    <div class="jt-v-align-middle"><?php the_sub_field('media_detail_media_svg'); ?></div>
                </div>
            <?php endif; ?>
            <?php if( $image_link ) : ?>
                <a class="media-image-link uk-position-absolute uk-width-1-1 uk-height-1-1" href="<?php echo $image_link['url']; ?>"></a>
            <?php endif; ?>
        </div>
        <?php if( get_sub_field('media_detail_video') ) : ?>
            <a class="media-video-modal uk-position-center uk-padding-large" data-video-id="<?php echo get_sub_field('media_detail_video'); ?>" href="#">
                <button class="uk-button uk-button-primary"><span uk-icon="play"></span></button>
            </a>
        <?php endif; ?>
    </div>
</div>