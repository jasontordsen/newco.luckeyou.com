
<?php $overlay = get_sub_field("media_detail_overlay_detail"); ?>

<div uk-scrollspy="repeat:true;" class="uk-card-body jt-z-1<?php echo $overlay ? ' ' : ' jt-v-align-middle'; ?>">
    <div>
        <!-- ICON -->
        <?php if( get_sub_field('media_detail_icon') ): $icon = get_sub_field('media_detail_icon'); ?>
            <p><img src="<?php echo $icon['url']; ?>" height="80" /></p>
        <?php endif; ?>

        <!-- SVG ICON -->
        <?php if( get_sub_field('media_detail_svg_icon') ): ?>
            <?php the_sub_field('media_detail_svg_icon'); ?>
        <?php endif; ?>
        
        <!-- CONTENT -->
        <?php if( get_sub_field('media_detail_content') ): ?>
            <div class="detail-content <?php echo get_sub_field("media_detail_invert_content") ? "uk-light" : "uk-dark"; ?>">
                <?php the_sub_field("media_detail_content"); ?>
            </div>
        <?php endif; ?>

        <!-- BUTTON -->
        <?php get_template_part( "templates/buttons" ); ?>
    </div>
</div>