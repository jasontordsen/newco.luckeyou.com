<section class="form-section">
    <div class="section-inner uk-background-primary">
        <?php  
            $post = get_sub_field("form_section_form");
            setup_postdata( $post ); 
            get_template_part("templates/posts/contact_form");
            wp_reset_postdata();
        ?>
    </div>
</section>