<section style="background-color:<?php the_sub_field("background_color"); ?>" class="slideshow-section uk-position-relative uk-visible-toggle" tabindex="-1" uk-slideshow="autoplay:true; autoplay-interval: 15000; ratio:false; animation:push;">
    <?php 
        $aspect_ratio = get_sub_field("slideshow_slider_aspect_ratio");
        if( !isset( $aspect_ratio ) || is_array($aspect_ratio) ) {
            $aspect_ratio = "40";
        }
    ?>
    <div class="jt-ar-<?php echo $aspect_ratio; ?>"></div>
    <ul class="uk-slideshow-items">
        <?php 
            $slideshow_post = get_sub_field('slideshow_slider_slideshow');

            if( $slideshow_post ): 
            $post = $slideshow_post;
            setup_postdata( $post );
        ?>
            <?php while( have_rows('section') ): the_row(); ?>
                <?php $layout = get_row_layout(); ?>
                <li style="background-color:white; bottom:auto;">
                    <?php get_template_part( "templates/sections/". $layout. "/" . $layout ); ?>
                </li>
            <?php endwhile; ?>
        <?php wp_reset_postdata(); endif; ?>
    </ul>
    <ul class="uk-position-bottom uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
</section>