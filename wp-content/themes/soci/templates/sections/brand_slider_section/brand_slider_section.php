<?php
    $slider_bg_color = get_sub_field("brand_slider_slider_background_color");
    $show_dropshadow = false;

    $overlap = get_sub_field('brand_slider_padding');
    if( !isset($overlap) ){
        $overlap = "default";
    }

    $justification = 'center';
    if( get_sub_field('brand_slider_justification') ) $justification = get_sub_field('brand_slider_justification');

    if( $slider_bg_color == "#ffffff" || $slider_bg_color == "#fff" || $slider_bg_color == "#FFF" || $slider_bg_color == "#FFFFFF" ){
        $show_dropshadow = true;
    }

    $corner_class = "jt-border-tl-30 jt-border-br-30";

    if($justification == 'left') $corner_class = 'jt-border-tr-30';
    if($justification == 'right') $corner_class = 'jt-border-tl-30';
?>
<section uk-scrollspy class="brand-slider-section brand-slider-<?php echo $justification; ?> padding-<?php echo $overlap; ?> uk-padding-large" style="background-color:<?php the_sub_field("background_color"); ?>" uk-scrollspy="target: .uk-grid > li; cls:uk-animation-fade; delay: 200">
    <div class="uk-padding uk-padding-remove-vertical<?php echo $show_dropshadow == true ? ' uk-box-shadow-medium' : ''; ?> <?php echo $corner_class; ?>" style="background-color:<?php echo $slider_bg_color; ?>">
        <div class="uk-position-relative uk-visible-toggle uk-padding-small uk-padding-remove-horizontal" tabindex="-1" uk-slider="pause-on-hover:false; autoplay:true;">
            <?php if( get_sub_field('brand_slider_heading')) : ?>
                <div class="uk-margin-bottom uk-margin-small-top">
                    <?php the_sub_field('brand_slider_heading'); ?>
                </div>
            <?php endif; ?>
            <?php $rows = count( get_sub_field('brand_slider_slide')); ?>
            <ul class="uk-padding-small uk-padding-remove-horizontal uk-slider-items <?php echo $rows < 3 ? 'uk-child-width-expand' : 'uk-child-width-1-3'; ?> <?php echo $rows < 4 ? 'uk-child-width-expand' : 'uk-child-width-1-4'; ?>@s <?php echo $rows < 6 ? 'uk-child-width-expand' : 'uk-child-width-1-6'; ?>@m uk-grid" uk-grid>
                <?php while( have_rows('brand_slider_slide') ): the_row(); ?>
                <li>
                    <?php $image = get_sub_field('brand_slider_slide_image'); ?>
                    <div class="uk-width-1-1 uk-height-1-1 uk-position-relative uk-flex uk-flex-middle uk-flex-center">
                        <img data-src="<?php echo $image['url']; ?>" uk-img />
                    </div>
                </li>
                <?php endwhile; ?>
            </ul>
        </div>
    </div>
</div>