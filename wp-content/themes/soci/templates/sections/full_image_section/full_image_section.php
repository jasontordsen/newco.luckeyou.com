<section style="background-color:<?php the_sub_field("background_color"); ?>" class="uk-padding">
    <div class="section-heading" uk-scrollspy="target: > *; cls:uk-animation-fade; delay:100;">
        <?php if( get_sub_field('full_image_heading') || get_sub_field('buttons') ) : ?>
            <div class="uk-padding uk-container">
                <?php if( get_sub_field('full_image_heading') ) : ?>
                    <?php the_sub_field('full_image_heading'); ?>
                <?php endif; ?>
                <?php get_template_part( "templates/buttons" ); ?>
            </div>
        <?php endif; ?>
    </div>
    <?php 
        $image = get_sub_field("full_image_image"); $image_size = '';
        if( get_sub_field('image_size') && is_string( get_sub_field('image_size') )){
            $image_size = get_sub_field('image_size');
        } 
    ?>
    <div class="uk-container <?php echo $image_size ?>">
        <img uk-img data-src="<?php echo $image['url']; ?>" width="100%" />
    </div>
</section>