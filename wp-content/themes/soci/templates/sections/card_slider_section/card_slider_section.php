
<?php 
    $image_aspect_ratio = get_sub_field('card_slider_image_aspect_ratio');
?>
        
<section class="card-slider-section" >
    <div class="uk-position-absolute uk-width-1-1" style="z-index:-1; bottom:0; height:33%; background-color:<?php the_sub_field("background_color"); ?>" ></div>
    <div uk-slider="pause-on-hover:false; autoplay:true; autoplay-interval: 8000; center: true">
        <div class="section-heading uk-padding-small uk-text-center">
            <div class="slide-nav uk-position-relative">
                <a class="uk-position-top-left uk-text-primary" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                <a class="uk-position-top-right uk-text-primary" href="#" uk-slidenav-next uk-slider-item="next"></a>
            </div>
            <?php the_sub_field('card_slider_heading'); ?>
            <?php get_template_part( "templates/buttons" ); ?>
        </div>
        <div class="uk-position-relative uk-visible-toggle" tabindex="-1">
            <a class="next-prev-thumb uk-position-center-left uk-text-primary" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
            <ul class="uk-grid-collapse uk-slider-items uk-child-width-1-2@s" uk-grid>
                <?php while( have_rows('card_slider_card') ): the_row(); ?>
                <li>
                    <div class="uk-card">

                        <!-- IMAGE -->
                        <?php if( get_sub_field("card_slider_card_image") ) : $image = get_sub_field('card_slider_card_image'); ?>
                            <div class="uk-card-media-top">
                                <div class="uk-background-cover jt-ar-<?php echo $image_aspect_ratio; ?>" data-src="<?php echo $image['url']; ?>" uk-img>
                                    <?php if( get_sub_field('card_slider_card_video') ) : ?>
                                    <div class="uk-position-center "><a class="uk-button uk-button-primary" data-video-id="<?php echo get_sub_field('card_slider_card_video'); ?>" href="#"><span uk-icon="play"></span></a></div>
                                    <?php endif; ?>
                                    <?php if( get_sub_field("card_slider_card_title") ) : ?>
                                        <div class="card-title uk-position-bottom-left jt-background-white uk-padding uk-padding-remove-vertical">
                                            <h3><?php the_sub_field("card_slider_card_title"); ?></h3>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endif; ?>

                        <div class="uk-card-body">
                            <?php if( get_sub_field("card_slider_card_stat") ) : ?>
                            <h1><?php the_sub_field("card_slider_card_stat"); ?></h1>
                            <?php endif; ?>

                            <?php if( get_sub_field("card_slider_card_description") ) : ?>
                            <p class="jt-small"><?php the_sub_field("card_slider_card_description"); ?></p>
                            <?php endif; ?>
                        </div>
                    </div>
                </li>

                <?php endwhile; ?>
            </ul>
            <a class="next-prev-thumb uk-position-center-right uk-text-primary" href="#" uk-slidenav-next uk-slider-item="next"></a>
        </div>
    </div>
</div>