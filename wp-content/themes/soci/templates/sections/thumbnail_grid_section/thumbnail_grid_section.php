<section style="background-color:<?php the_sub_field("background_color"); ?>" class="thumbnail-grid-section uk-padding">
    <div uk-scrollspy="target: > *; cls:uk-animation-fade; delay:200;" class="section-heading uk-margin-large-bottom">
        <?php if( get_sub_field('thumbnail_grid_header') ) : ?>
            <?php the_sub_field("thumbnail_grid_header"); ?>
        <?php endif; ?>
        <?php get_template_part( "templates/buttons" ); ?>
    </div>
    <div uk-scrollspy="target: > a .uk-overflow-hidden; cls:uk-animation-fade; delay: 200" class="uk-grid-small uk-child-width-1-2@s uk-child-width-1-3@m uk-child-width-1-4@l" uk-grid>
        <?php while( have_rows('thumbnail_grid_item') ): the_row(); ?>
            <?php 
                $index = get_row_index();
                $image = get_sub_field("thumbnail_grid_item_image");  
            ?>

            <!-- MODAL -->
            <div id="thumbnail-modal-<?php echo $index; ?>" class="uk-flex-top uk-modal" uk-modal>
                <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">

                    <button class="uk-modal-close-default uk-button-primary" type="button" uk-close></button>

                    <div class="uk-grid-collapse uk-child-width-1-2@m" uk-grid>
                        <div>
                            <div class="jt-ar-100 uk-background-cover" data-src="<?php echo $image['url']; ?>" uk-img></div>
                        </div>
                        <div class="uk-padding uk-padding-remove-vertical uk-padding-remove-right">
                            <div class="jt-v-align-middle">
                                <?php the_sub_field('thumbnail_grid_item_detail'); ?>
                                <?php if( get_sub_field('thumbnail_grid_item_social_links') ) : ?>
                                    <ul class="uk-iconnav">
                                        <?php while( have_rows('thumbnail_grid_item_social_links') ): the_row(); ?>
                                            <?php $link = get_sub_field('link'); ?>
                                            <li class="uk-margin-small-right"><a href="<?php echo $link['url']; ?>" uk-icon="icon: <?php echo get_sub_field('icon'); ?>"></a></li>
                                        <?php endwhile; ?>
                                    </ul>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        

            <!-- TUMBNAIL -->
            <a class="uk-display-block" href="#thumbnail-modal-<?php echo $index; ?>" uk-toggle>
                <?php if(!empty($image)) : ?>
                <div class="uk-overflow-hidden uk-animation-toggle">
                    <div class="uk-animation-kenburns uk-background-cover jt-ar-100" data-src="<?php echo $image['url']; ?>" uk-img></div>
                </div>
                <?php endif; ?>
                <div class="caption uk-card uk-card-body uk-padding-small uk-padding-remove-left">
                    <div class="caption-inner primary-border-left"><?php the_sub_field("thumbnail_grid_item_caption"); ?></div>
                </div>
            </a>
        <?php endwhile; ?>
    </div>
</section>