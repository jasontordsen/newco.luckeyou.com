<?php if(get_field('event_title')) : ?><h3 class="uk-margin-remove"><strong><?php the_field('event_title'); ?></strong></h3><?php endif; ?>

<?php if(get_field('event_start_date')) : ?>
    <h4 class="uk-margin-remove">
        <?php 
            $start_date = DateTime::createFromFormat('Ymd', get_field('event_start_date') ); 
            echo $start_date->format('F d, Y'); 
        ?>
        <?php if(get_field('event_end_date')) : ?>
        - <?php     
            $end_date = DateTime::createFromFormat('Ymd', get_field('event_end_date') ); 
            echo $end_date->format('F d, Y'); 
        ?>
        <?php endif; ?>
    </h4>
<?php endif; ?>

<?php if(get_field('event_location')) : ?><h4 class="uk-margin-remove"><strong><?php the_field('event_location'); ?></strong></h4><?php endif; ?>
