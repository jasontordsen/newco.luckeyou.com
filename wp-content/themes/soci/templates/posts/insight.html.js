<li class="post insight-post uk-margin-bottom">
    <a href="{{permalink}}" style="height:100%" class="uk-card-wrap uk-display-block uk-card uk-card-default uk-animation-toggle">    
        <div class="border"></div>
        <div class="hover-bg"></div>
        {{#if thumbnail}}
            <div class="uk-photo-link uk-display-block">
                <div class="uk-card-media-top">
                    <div class="uk-width-1-1@s uk-background-cover">
                        <div class="uk-background-cover jt-ar-5625" data-src="{{thumbnail}}" uk-img></div>
                    </div>
                </div>
            </div>
        {{/if}}
        <div class="uk-card-body">
            <div class="uk-text-primary"><span>{{tax}}</span></div>
            <h3>{{title}}</h3>
            {{excerpt}}<span href="{{permalink}}"><span class="readmore"> Read more</span></span>
        </div>    
    </a>
</li>