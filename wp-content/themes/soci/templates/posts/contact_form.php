<?php $image = get_field('contact_form_image'); ?>

<form id="form-<?php echo $post->ID; ?>" action="<?php the_field('contact_form_handler_url'); ?>" method="post" class="uk-light">
    <div class="uk-child-width-expand@m" uk-grid>
        <div>
            <div class="uk-padding jt-v-align-middle">
                <h1><?php the_field("contact_form_heading"); ?></h1>
                <?php while( have_rows('contact_form_fields') ) : the_row(); ?>
                    <?php if( get_sub_field('type') === "checkbox" ) : ?>
                        <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid">
                            <label><input class="uk-checkbox uk-margin-small-right" type="checkbox" checked> <?php the_sub_field("label_after"); ?></label>
                        </div>
                    <?php else : ?>
                        <label class="jt-text-white"><?php the_sub_field("label_before"); ?></label>
                        <input placeholder="<?php the_sub_field("placeholder"); ?>" type="<?php the_sub_field('type'); ?>" name="<?php the_sub_field('name'); ?>" required>
                        <label class="jt-text-white"><?php the_sub_field("label_after"); ?></label>
                    <?php endif; ?>
                <?php endwhile; ?>
                <div>
                    <div class="g-recaptcha" data-sitekey="6LcR5qoUAAAAAHhQ_c52AbposPbjdPKSBqVRz2hK" data-callback="recaptchaOnSubmit" data-size="invisible"></div>
                    <button class="uk-margin-top uk-button uk-button-primary jt-text-link"><strong><?php the_field('contact_form_submit_label'); ?></strong></button>
                </div>
            </div>
        </div>
        <?php if($image) : ?>
            <div class="uk-background-contain" style="background-position:center bottom; min-height:100%;" uk-img data-src="<?php echo $image['url']; ?>">
                <div class="uk-width-1-1 jt-ar-100"></div>
            </div>
        <?php endif; ?>
    </div>
</form>