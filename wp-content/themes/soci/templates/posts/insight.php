
<?php
    $permalink = get_the_permalink();
    if( get_field('external_link') ){
        $permalink = get_field('external_link');
        $permalink = $permalink['url'];
    }
    $postId = get_the_ID();
?>

<li class="post insight-post uk-margin-bottom">
    <a href="<?php echo $permalink; ?>" style="height:100%" class="uk-card-wrap uk-display-block uk-card uk-card-default uk-animation-toggle">
        <div class="border"></div>
        <div class="hover-bg"></div>
        <?php if( has_post_thumbnail() ) : ?>
            <div class="uk-photo-link uk-display-block">
                <div class="uk-card-media-top">
                    <div class="uk-width-1-1@s uk-overflow-hidden">
                        <div class="uk-background-cover jt-ar-5625 uk-animation-kenburns" data-src="<?php the_post_thumbnail_url(); ?>" uk-img></div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <div class="uk-card-body">
            <div class="uk-text-primary">
                <?php
                    $terms = wp_get_post_terms( $postId, 'insight_type', array('fields' => 'names') );
                    echo implode(" | ", $terms);
                ?>
            </div>
            <h3><?php the_title(); ?></h3>
            <?php echo get_the_excerpt(); ?> <span href="<?php echo $permalink; ?>"><span class="readmore"> Read more</span></span>
        </div>
    </a>
</li>