<li class="post event-post uk-margin-bottom">
    <a style="height:100%" class="uk-display-block uk-card uk-card-default" href="{{permalink}}">
        <div class="border"></div>
        <div class="hover-bg"></div>
        {{#if thumbnail}}
            <div class="uk-card-media-top">
                <div class="uk-width-1-1@s uk-background-cover">
                    <div class="uk-background-cover jt-ar-5625" data-src="{{thumbnail}}" uk-img></div>
                </div>
            </div>
        {{/if}}
        <div class="uk-card-body">
            <h3>{{title}}</h3>

            {{#if date}}
                <h6 class="uk-text-muted">{{date}}</h6>
            {{/if}}

            {{#if location}}
                <h6 class="uk-text-muted">{{location}}</h6>
            {{/if}}

            {{excerpt}}<span class="readmore"> Read more</span>

            {{#if event_icon}}
                <p class="uk-margin-top"><img height="80" src="{{event_icon}}" /></p>
            {{/if}}
        </div>
    </a>
</li>