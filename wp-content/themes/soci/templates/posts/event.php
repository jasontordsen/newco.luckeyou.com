
<?php 
    $permalink = get_the_permalink(); 
    if( get_field('external_link') ){
        $permalink = get_field('external_link');
        $permalink = $permalink['url'];
    }
?>

<li class="post event-post uk-margin-bottom">
    <a href="<?php echo $permalink; ?>" style="height:100%" class="uk-display-block uk-card uk-card-default uk-animation-toggle">
        <div class="border"></div>
        <div class="hover-bg"></div>
        <?php if( has_post_thumbnail() ) : ?>
            <div class="uk-card-media-top">
                <!-- IMAGE -->
                <div class="uk-width-1-1@s uk-overflow-hidden">
                    <div class="uk-background-cover jt-ar-5625 uk-animation-kenburns" data-src="<?php the_post_thumbnail_url(); ?>" uk-img></div>
                </div>
            </div>
        <?php endif; ?>
        <div class="uk-card-body">
            <?php if(get_field('event_title')) : ?>
                <h3><?php the_field('event_title'); ?></h3>
            <?php else: ?>
                <h3><?php the_title(); ?></h3>
            <?php endif; ?>

            <?php if(get_field('event_start_date')) : ?>
                <h6 class="uk-text-muted"><?php 
                    $start_date = DateTime::createFromFormat('Ymd', get_field('event_start_date') ); 
                    echo $start_date->format('F d, Y'); 
                ?>
                <?php if(get_field('event_end_date')) : ?>
                - <?php     
                    $end_date = DateTime::createFromFormat('Ymd', get_field('event_end_date') ); 
                    echo $end_date->format('F d, Y');
                ?>
                <?php endif; ?></h6>
            <?php endif; ?>

            <?php if(get_field('event_location')) : ?>
                <h6 class="uk-text-muted"><?php the_field('event_location'); ?></h6>
            <?php endif; ?>

            <?php echo get_the_excerpt(); ?><span class="readmore"> Read more</span>

            <?php if(get_field('event_icon')) : $icon = get_field('event_icon'); ?>
                <p class="uk-margin-top"><img height="80" src="<?php echo $icon['url']; ?>" /></p>
            <?php endif; ?>
        </div>
    </a>
</li>