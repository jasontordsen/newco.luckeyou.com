
<?php 
    $permalink = get_the_permalink(); 
    if( get_field('external_link') ){
        $permalink = get_field('external_link');
        $permalink = $permalink['url'];
    }
?>

<li class="post post-post uk-margin-bottom">
    <a href="<?php echo $permalink; ?>" style="height:100%" class="uk-display-block uk-card uk-card-default uk-animation-toggle">
        <div class="border"></div>
        <div class="hover-bg"></div>
        <?php if( get_the_post_thumbnail_url() ) : ?>
            <div class="uk-card-media-top">
                <div class="uk-width-1-1@s uk-overflow-hidden">
                    <div class="uk-background-cover jt-ar-5625 uk-animation-kenburns" data-src="<?php the_post_thumbnail_url(); ?>" uk-img></div>
                </div>
            </div>
        <?php endif; ?>
        <div class="uk-card-body">
            <?php echo get_the_date(); ?>
            <h3><?php the_title(); ?></h3>
            <?php echo get_the_excerpt(); ?><span class="readmore"> Read more</span>
        </div>
    </a>
</li>