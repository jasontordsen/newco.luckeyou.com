<?php while ( have_posts() ) : the_post(); ?>

    <?php 
        $image_background_color = "#000";
        $image_opacity = .8;

        if( get_field("post_header_background_color") ){
            $image_background_color = get_field("post_header_background_color");
        }

        if( get_field("post_header_image_opacity") ){
            $image_opacity = get_field("post_header_image_opacity");
        }
    ?>

    <?php if( !get_field('insight_options_hide_post_header') ) : ?>
        <?php if( has_post_thumbnail() ) : ?>
            <div class="featured-image uk-margin-bottom" style="background-color:<?php echo $image_background_color; ?>">
                <div class="jt-ar-40">
                    <div class="uk-position-absolute uk-width-1-1 uk-background-cover uk-height-1-1" style="opacity:<?php echo $image_opacity; ?>" data-src="<?php the_post_thumbnail_url('full'); ?>" uk-img></div>
                    <div class="uk-overlay uk-position-center-left uk-width-1-1 jt-z-1 uk-light">
                        <div class="uk-container">
                            <h1 class="uk-width-1-2"><strong><?php the_title(); ?></strong></h1>
                        </div>
                    </div>
                </div>
            </div>
        <?php else : ?>
            <div class="uk-padding-large uk-background-secondary uk-light uk-margin-bottom">
                <div class="uk-container uk-margin-large-top">
                    <h1 class="uk-width-3-4"><strong><?php the_title(); ?></strong></h1>
                </div>
            </div>
        <?php endif; ?>
    <?php else: ?>
    <?php endif; ?>

    <div id="post-content" class="uk-position-relative">
        <?php get_template_part( "templates/social" ); ?>

        <?php if( $post->post_type === 'event') : ?>
            <div class="uk-container">
                <div class="uk-padding uk-padding-remove-horizontal uk-padding-remove-bottom">
                    <?php get_template_part( "templates/event_details" ); ?>
                </div>
            </div>
        <?php endif; ?>

        <?php if( get_the_content() ) : ?>
            <div class="uk-container <?php echo $post->post_type == "post" ? ' uk-container-small' : ''; ?>">
                <div class="uk-padding uk-padding-remove-horizontal">
                    <?php the_content(); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
<?php endwhile; ?>