var sections = [], didscroll = false, didresize = true, laststeptime = 0, mousedidmove = null, currentframe = 0, resizeto = null, slideshows = [];

/*======== READY ==========
===================================*/
$(function() {
    var current_video_id = null;

    var video_modal = $('#modal-media-video');

    // $("#main-nav").on("show", function(e){
    //     var _t = $(this);

    //     setTimeout( function(){
    //         $("#main-nav").trigger("hide");
    //     },3000);
    // });

    $('a[href^="#"]').on('click', function(e) {
        e.preventDefault();

        var href = $(this).attr('href'),
        anchor = null;

        if( href.length > 1 ){
            anchor = $( "a" + href );
        }

        if(anchor){
            const scrollTop =
                anchor.offset().top - 140;

            $('html,body').animate({ scrollTop });
        }
    });

    video_modal.on('show', function(){
        console.log("show", current_video_id);

        var url = '';

        if( Number(current_video_id) ){
            url = 'https://player.vimeo.com/video/' + current_video_id+ '?autoplay=1';
        } else {
            url = 'https://www.youtube.com/embed/' + current_video_id + '?rel=0&start=0&autoplay=1';
        }

        $(this).find("iframe").eq(0).attr('src',url);
    });

    video_modal.on('hide', function(){
        console.log("hide", current_video_id);

        current_video_id = null;

        $(this).find("iframe").eq(0).attr('src','');
    });

    $('a[data-video-id]').click(function(e){
        e.preventDefault();
        current_video_id = $(this).attr('data-video-id');
        UIkit.modal(video_modal).show();
    });

    if(typeof(notification) !== 'undefined')
    UIkit.notification( "<span uk-icon='bell' class='uk-text-primary uk-margin-small-right'></span><span class='uk-text-small'>" + notification.message + "</span>", {pos: 'top-left'});

    $('section.slideshow-section').each(function(){
        var slideshow = {};

        slideshow.$el = $(this);
        slideshow.items = $(this).find(".uk-slideshow-items>li");

        slideshows.push(slideshow);
    });

    $(window).on( "scroll", onScroll );
    $(window).on( "resize", onResize );

    didresize = true;
    didscroll = true;

    window.requestAnimationFrame(step);
});

function onDidResize(){
    console.log("onDidResize");

    console.log(slideshows);

    $.each( slideshows, function(i,slideshow){ 

        var height = 0;
        $.each(slideshow.items, function(ii,item){
            height = Math.max( height, $(item).height() );
        });

        slideshow.$el.height(height);
    });
}

function onDidScroll(){
    console.log("onDidScroll");
}

/*======== EVENT HANDLERS ==========
===================================*/

function onScroll( _e ){
	didscroll = true;
}

function onResize( _e ){
	clearTimeout(resizeto);
    resizeto = setTimeout(function(){ didresize = true; }, 500 );
}

/*======== ANIMATION LOOP ==========
===================================*/
function step( time ){
	if( Math.round(currentframe)%20 === 0 ){
		if(didresize){
			onDidResize();
			didscroll = true;
			didresize = false;
		}
	}

	if( Math.round(currentframe)%20 === 0 ){
		if(didscroll){
			onDidScroll();
			didscroll = false; 
		}
	}

	laststeptime = time;
	currentframe++;
	window.requestAnimationFrame(step);
}