<?php get_header(); ?>

<!-- NEX PREV -->
<?php get_template_part('templates/post_next_prev'); ?>

<!-- BREADCRUMB -->
<?php get_template_part('templates/post_breadcrumb'); ?>

<!-- CONTENT -->
<?php get_template_part( "templates/post_content" ); ?>

<!-- SECTIONS -->
<?php get_template_part( "templates/sections" ); ?>

<!-- CONTACT FORM -->
<?php 
    $show_contact_form = true; 
    if( get_field( 'show_contact_form' ) === false ){ $show_contact_form = false; }
    if( $show_contact_form === true ) : if( get_field("event_form", 'option') ) : 
?>
    <section class="form-section">
        <div class="section-inner uk-background-primary">
            <?php  
                $post = get_field("event_form", 'option');
                setup_postdata( $post ); 
                get_template_part("templates/posts/contact_form");
                wp_reset_postdata();
            ?>
        </div>
    </section>
<?php endif; endif; ?>

<?php get_footer(); ?>