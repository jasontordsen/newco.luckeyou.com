<!doctype html>

<html class="no-js">

	<head>
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" type="text/css">

		<meta charset="<?php bloginfo('charset'); ?>">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<?php wp_head(); ?>

		<?php if( is_front_page() && get_field("notification",'option') ) : $notification = get_field("notification",'option'); ?>

		<!-- NOTIFICATION -->
		<script type="text/javascript">
			var link_val = JSON.parse( '<?php echo json_encode( $notification['link'] ); ?>' );
			var notification = { message: "<?php echo $notification['message']; ?>...<a class='uk-text-primary' href='"+link_val.url+"'><b>READ MORE</b></a>", link: link_val }
		</script>
		<?php endif; ?>
	</head>
	<body <?php body_class(); ?>>
		<?php get_template_part("templates/navigation/mobile_navigation"); ?>

		<?php get_template_part("templates/navigation/main_navigation"); ?>
		
		<!-- wrapper -->
		<div id="main-content" class="container-fluid">