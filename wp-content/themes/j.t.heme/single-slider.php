<?php get_header(); ?>

<div class="uk-position-relative uk-visible-toggle" tabindex="-1" uk-slideshow="ratio:false;">

    <ul class="uk-slideshow-items">
        <?php while( have_rows('section') ): the_row(); ?>
        <?php $layout = get_row_layout(); ?>
        <li style="background-color:white; bottom:auto;">
            <?php get_template_part( "templates/sections/". $layout. "/" . $layout ); ?>
        </li>
        <?php endwhile; ?>
    </ul>

    <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
    <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
</div>

<?php get_footer(); ?>