<section style="background-color:<?php the_sub_field("background_color"); ?>" class="uk-padding jt-background-light">
    <div class="uk-margin-bottom">
        <?php the_sub_field('image_gallery_heading_content'); ?>
        <?php get_template_part( "templates/buttons" ); ?>
    </div>
    <?php 
        $images = get_sub_field('image_gallery_images');
        $first = array_shift($images); 
    ?>
        <div class="uk-grid uk-grid-collapse uk-child-width-1-2@s" uk-grid>
            <div>
                <div class="uk-width-1-1 jt-ar-100 uk-background-cover" data-src="<?php echo $first['url']; ?>" uk-img></div>
            </div>
            <div uk-grid class="uk-grid uk-grid-collapse">
                <?php foreach( $images as $image ): ?>
                    <div class="uk-width-1-2@s">
                        <div class="uk-width-1-1 jt-ar-100 uk-background-cover" data-src="<?php echo $image['url']; ?>" uk-img></div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
</section>