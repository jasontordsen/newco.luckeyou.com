<section style="background-color:<?php the_sub_field("background_color"); ?>" class="uk-padding">
    <div uk-scrollspy="target: > *; cls:uk-animation-fade; delay:200;" class="section-heading">
        <?php if( get_sub_field('detail_grid_heading_content') ) : ?>
            <?php the_sub_field('detail_grid_heading_content'); ?>
        <?php endif; ?>
        <?php get_template_part( "templates/buttons" ); ?>
    </div>
    <div uk-scrollspy="target: > div > div; cls:uk-animation-slide-bottom; delay: 200" class="uk-grid uk-child-width-1-2 uk-child-width-1-3@s uk-child-width-1-4@l uk-padding uk-padding-remove-horizontal uk-padding-remove-bottom" uk-grid>
        <?php while( have_rows('detail_grid_item') ): the_row(); ?>
            <div>
                <div class="uk-padding uk-padding-remove-left uk-padding-remove-vertical">
                    <?php $image = get_sub_field("detail_grid_item_icon"); ?>
                    <p><img src="<?php echo $image['url']; ?>" alt="logo" /></p>
                    <?php the_sub_field("detail_grid_item_content"); ?>
                </div>
            </div>
        <?php endwhile; ?>
    </div>
</section>