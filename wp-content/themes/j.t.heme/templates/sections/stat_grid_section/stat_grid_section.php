<section style="background-color:<?php the_sub_field("background_color"); ?>" class="uk-padding">
    <?php $columns = get_sub_field('stat_grid_columns'); ?>

    <div uk-scrollspy="target: > *; cls:uk-animation-fade; delay:200;" class="section-heading">
        <?php if( get_sub_field('stat_grid_heading') ) : ?>
            <?php the_sub_field('stat_grid_heading'); ?>
        <?php endif; ?>
        <?php get_template_part( "templates/buttons" ); ?>
    </div>

    <div uk-scrollspy="target: > div; cls:uk-animation-slide-bottom; delay: 200" class="uk-grid uk-child-width-1-2@s uk-child-width-1-<?php echo $columns; ?>@m uk-padding uk-padding-remove-horizontal uk-padding-remove-bottom">
        <?php while( have_rows('stat_grid_item') ): the_row(); ?>
            <div class="uk-margin-large-bottom">
                <?php 
                    $title = "&nbsp;";
                    if( get_sub_field("stat_grid_item_title") ){
                        $title = get_sub_field("stat_grid_item_title");
                    }
                ?>
                <h4 class="uk-margin-remove"><b><?php echo $title; ?></b></h4>
                <h1 style="font-size:100px; font-weight:700" class="uk-margin-remove uk-text-primary"><?php the_sub_field("stat_grid_item_stat"); ?></h1>
                <?php the_sub_field("stat_grid_item_description"); ?>
            </div>
        <?php endwhile; ?>
    </div>
</section>