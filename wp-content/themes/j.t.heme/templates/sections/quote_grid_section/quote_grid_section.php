<!-- Quote Grid -->
<?php $num_rows = count( get_sub_field('quote_grid_quote') ); ?>

<section uk-scrollspy style="background-color:<?php the_sub_field("background_color"); ?>" class="<?php echo 'rows-'.$num_rows; ?> uk-padding-large uk-padding-remove-horizontal quote-grid-section uk-background-secondary uk-light">
    <div class="uk-margin-small-top quote-icon uk-position-top-right">
        <svg class="uk-margin-large-right jt-v-align-middle" width="120" viewBox="0 0 267 153" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M37.1725 0.000244141C36.7409 0.000244141 36.358 0.277142 36.2228 0.687019L0.95525 107.616C0.423732 107.64 0 108.078 0 108.615V151.998C0 152.551 0.447716 152.998 1 152.998H38.1608C38.7131 152.998 39.1608 152.551 39.1608 151.998V108.615C39.1608 108.063 38.7131 107.615 38.1608 107.615H27.462L45.5381 1.16766C45.6418 0.55722 45.1714 0.000244141 44.5522 0.000244141H37.1725Z" fill="#76BA4D"/>
            <path fill-rule="evenodd" clip-rule="evenodd" d="M93.9545 0.000244141C93.523 0.000244141 93.1401 0.277142 93.0049 0.687019L57.7374 107.616C57.2041 107.637 56.7783 108.077 56.7783 108.615V151.998C56.7783 152.551 57.226 152.998 57.7783 152.998H94.939C95.4913 152.998 95.939 152.551 95.939 151.998V108.615C95.939 108.063 95.4913 107.615 94.939 107.615H84.2441L102.32 1.16766C102.424 0.55722 101.954 0.000244141 101.334 0.000244141H93.9545Z" fill="#76BA4D"/>
            <path fill-rule="evenodd" clip-rule="evenodd" d="M229.698 152.998C230.13 152.998 230.513 152.721 230.648 152.312L265.915 45.3817C266.447 45.3585 266.871 44.9201 266.871 44.3827L266.871 0.999985C266.871 0.447693 266.423 -1.52979e-05 265.871 -1.53462e-05L228.71 -1.85949e-05C228.158 -1.86432e-05 227.71 0.447689 227.71 0.999981L227.71 44.3827C227.71 44.935 228.158 45.3827 228.71 45.3827L239.409 45.3827L221.332 151.831C221.229 152.441 221.699 152.998 222.318 152.998L229.698 152.998Z" fill="#76BA4D"/>
            <path fill-rule="evenodd" clip-rule="evenodd" d="M172.919 153C173.351 153 173.734 152.723 173.869 152.313L209.137 45.3827C209.669 45.3598 210.093 44.9212 210.093 44.3836L210.093 1.0009C210.093 0.448608 209.645 0.000884971 209.093 0.000884922L171.932 0.000881674C171.38 0.000881625 170.932 0.448605 170.932 1.00088L170.932 44.3836C170.932 44.9359 171.38 45.3836 171.932 45.3836L182.63 45.3836L164.553 151.832C164.45 152.443 164.92 153 165.539 153L172.919 153Z" fill="#76BA4D"/>
        </svg>
    </div>  
    <div class="uk-padding uk-padding-remove-vertical">
        <?php the_sub_field("quote_grid_heading"); ?>
        <?php get_template_part( "templates/buttons" ); ?>
    </div>
    <div class="uk-padding uk-padding-remove-bottom uk-padding-remove-right">
        <ul class="uk-child-width-1-2@m uk-child-width-expand@l" uk-grid uk-scrollspy="target: > li; cls:uk-animation-fade; delay: 200">
            
            <?php while( have_rows('quote_grid_quote') ): the_row(); ?>
                <li class="grid-item uk-margin-small-bottom">
                    <div class="grid-item-inner uk-padding-large uk-padding-remove-vertical uk-padding-remove-left">
                        <?php the_sub_field("quote_grid_quote_content"); ?>
                        <?php the_sub_field("quote_grid_quote_author"); ?>
                        <?php $icon = get_sub_field("quote_grid_quote_icon"); ?>
                        <img src="<?php echo $icon['url']; ?>" alt="logo" />
                    </div>
                </li>
            <?php endwhile; ?>
        </ul>
    </div>
</section>