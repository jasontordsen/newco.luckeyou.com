
<?php $video = get_sub_field('full_video_video'); ?>

<section style="background-color:<?php the_sub_field("background_color"); ?>" class="uk-background-cover uk-padding-large uk-padding-remove-bottom">
    <div class="jt-ar-5625">
        <div class="uk-padding uk-padding-remove-vertical uk-position-absolute uk-width-1-1 uk-height-1-1">
            <?php if( is_numeric( $video ) ) : ?>
                <iframe src="https://player.vimeo.com/video/<?php echo $video ?>" width="100%" height="100%" frameborder="0" uk-video></iframe>
            <?php else :  ?>
                <iframe src="https://www.youtube.com/embed/<?php echo $video ?>?rel=0&start=0&autoplay=1" width="100%" height="100%" frameborder="0" allowfullscreen></iframe>
            <?php endif; ?>
        </div>
    </div>
</section>