<section style="background-color:<?php the_sub_field("background_color"); ?>" class="uk-padding">
    <?php $image = get_sub_field("full_image_image"); ?>
    <img uk-img data-src="<?php echo $image['url']; ?>" width="100%" />
</section>