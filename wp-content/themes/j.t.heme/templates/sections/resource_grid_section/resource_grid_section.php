<?php 
    global $num_posts_per_page;
    
    $post_types = get_sub_field( 'resource_grid_post_type' );
    $categories = get_sub_field( 'resource_grid_category' );
    $solutions = get_sub_field( 'resource_grid_solution' );
    $insight_types = get_sub_field( 'resource_grid_insight_type' );
    $event_types = get_sub_field( 'resource_grid_event_type' );
    $columns = get_sub_field( 'resource_grid_columns' );
    $tax_query = array('relation' => 'AND');

    if( !empty( $insight_types ) ) {
        $terms = array();
        $term_objects = array();

        foreach($insight_types as $insight_type){
            array_push($terms,$insight_type->term_id);
            array_push($term_objects, array('id'=>$insight_type->term_id, 'name'=>$insight_type->name));
        }

        array_push( $tax_query, 
            array(
                'taxonomy' => 'insight_type',
                'field' => 'term_id',
                'terms' => $terms,
                'term_objects' => $term_objects
            )
        );
    }

    if( !empty( $event_types ) ) {
        $terms = array();
        $term_objects = array();

        foreach($event_types as $event_type){
            array_push($terms,$event_type->term_id);
            array_push($term_objects, array('id'=>$event_type->term_id, 'name'=>$event_type->name));
        }

        array_push( $tax_query, 
            array(
                'taxonomy' => 'event_type',
                'field' => 'term_id',
                'terms' => $terms,
                'term_objects' => $term_objects
            )
        );
    }

    if( !empty( $categories ) ) {
        $terms = array();
        $term_objects = array();

        foreach($categories as $category){
            array_push($terms,$category->term_id);
            array_push($term_objects, array('id'=>$category->term_id, 'name'=>$category->name));
        }

        array_push( $tax_query, 
            array(
                'taxonomy' => 'category',
                'field' => 'term_id',
                'terms' => $terms,
                'term_objects' => $term_objects
            )
        );
    }

    if( !empty( $solutions ) ) {
        $terms = array();
        $term_objects = array();

        foreach($solutions as $solution){
            array_push($terms,$solution->term_id);
            array_push($term_objects, array('id'=>$solution->term_id, 'name'=>$solution->name));
        }

        array_push( $tax_query, 
            array(
                'taxonomy' => 'solution',
                'field' => 'term_id',
                'terms' => $terms,
                'term_objects' => $term_objects
            )
        );
    }

    $meta_key = '';
    $orderby = "date";
    $order = "DESC";
    $meta_query = "";

    if( $post_types === "event" || is_array($post_types) && array_search("event",$post_types) !== FALSE ){
        $today = date('Ymd');
        $meta_key = 'event_start_date';
        $orderby = "meta_value_num";
        $order = "ASC";
        $meta_query = array(
            array(
                'key'		=> 'event_start_date',
                'compare'	=> '>=',
                'value'		=> $today,
            )
        );
    }

    $args = array(
        'numberposts'      => $num_posts_per_page,
        'post_type'        => $post_types,
        'meta_query'       => $meta_query,
        'tax_query'        => $tax_query,
        'meta_key'	       => $meta_key,
        'orderby'	       => $orderby,
        'order'		       => $order
    );

    $posts = get_posts( $args );
?>


<section style="background-color:<?php the_sub_field("background_color"); ?>" class="resource-grid-section uk-padding uk-padding-remove-horizontal" >
    <script>
        var post_types = JSON.parse( '<?php echo json_encode( $post_types ); ?>' );
        var tax_query = JSON.parse( '<?php echo json_encode( $tax_query ); ?>' );
        var meta_query = JSON.parse( '<?php echo json_encode( $meta_query ); ?>' );
    </script>
    <div class="boundary-align uk-panel">
        <button class="uk-button uk-button-secondary" type="button"><b class="uk-margin-small-right">Filter</b><span uk-navbar-toggle-icon></span></button>
        <div class="filters uk-margin-remove uk-background-secondary uk-light" uk-drop="delay-hide:0; pos: bottom-justify; boundary: .boundary-align; boundary-align: true">
            <div id="taxonomy-menu">
                <?php if( !empty( $tax_query ) ) : ?>
                    <div class="uk-flex uk-flex-center">
                        <?php foreach( $tax_query as $tax): ?>
                            <?php if( is_array($tax) && $tax['taxonomy'] && $tax['term_objects'] && count( $tax['term_objects'] ) > 1 ) : ?>
                                <div class="uk-text-left uk-padding-large">
                                    <h6 class="uk-text-uppercase uk-text-primary underline underline-20 underline-2"><strong><?php echo ucwords( str_replace('_', ' ', $tax['taxonomy'] ) ); ?></strong></h6>
                                    <ul class="terms-list uk-margin-small-top uk-nav uk-nav-default" uk-nav>
                                        <?php foreach($tax['term_objects'] as $term) : ?>
                                        <li id="tax-term-<?php echo $term['id']; ?>">
                                            <a data-term-id="<?php echo $term['id']; ?>" href="#">
                                                <h4 class="uk-margin-remove"><b><?php echo $term['name']; ?></b> <!--<span class='count'>00</span>--></h4>
                                            </a>
                                        </li>
                                        <?php endforeach; ?>
                                    </ul>               
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <ul id="posts-container" uk-scrollspy="target: > li; cls:uk-animation-scale-up; delay: 100" class="uk-grid uk-grid-small uk-child-width-1-2@s uk-child-width-1-<?php echo $columns; ?>@m uk-padding" uk-grid>
        <?php foreach($posts as $post) : setup_postdata( $post ); ?>
        <?php get_template_part( "templates/posts/" . $post->post_type ); ?>
        <?php wp_reset_postdata(); endforeach; ?>
    </ul>
    <div class="uk-text-center">
        <button id="loadmore" class="uk-button uk-button-primary">See More</button>
    </div>
</section>