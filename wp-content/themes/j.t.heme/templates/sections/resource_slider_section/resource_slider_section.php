<?php 
    $post_types = get_sub_field( 'resource_slider_post_type' );
    $categories = get_sub_field( 'resource_slider_category' );
    $solutions = get_sub_field( 'resource_slider_solution' );
    $insight_types = get_sub_field( 'resource_slider_insight_type' );
    $event_types = get_sub_field( 'resource_slider_event_type' );
    $taxonomies = array('relation' => 'OR');

    if( !empty( $categories ) ) {
        array_push( $taxonomies, 
            array(
                'taxonomy' => 'category',
                'field' => 'term_id',
                'terms' => $categories
            )
        );
    }
    if( !empty( $solutions ) ) {
        array_push( $taxonomies, 
            array(
                'taxonomy' => 'solution',
                'field' => 'term_id',
                'terms' => $solutions
            )
        );
    }
    if( !empty( $insight_types ) ) {
        array_push( $taxonomies, 
            array(
                'taxonomy' => 'insight_type',
                'field' => 'term_id',
                'terms' => $insight_types
            )
        );
    }
    if( !empty( $event_types ) ) {
        array_push( $taxonomies, 
            array(
                'taxonomy' => 'event_type',
                'field' => 'term_id',
                'terms' => $event_types
            )
        );
    }

    $args = array(
        'numberposts'      => 6,
        'orderby'          => 'date',
        'order'            => 'DESC',
        'post_type'        => $post_types,
        'tax_query'        => $taxonomies
    );

    $posts = get_posts( $args );
?>

<section style="background-color:<?php the_sub_field("background_color"); ?>" class="uk-padding">
    <div class="uk-padding-remove-top uk-padding-small uk-padding-remove-horizontal uk-margin-bottom">
        <?php the_sub_field('resource_slider_heading'); ?>
    </div>

    <div uk-slider>
        <a class="uk-position-center-left uk-text-secondary" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
        <a class="uk-position-center-right uk-text-secondary" href="#" uk-slidenav-next uk-slider-item="next"></a>
        <div class="uk-position-relative uk-visible-toggle" tabindex="-1">
            <ul class="uk-slider-items uk-child-width-1-2@s uk-child-width-1-3@m uk-grid uk-grid-small" uk-grid>
                <?php 
                    while( have_rows('resource_slider_resource') ): the_row();

                    $post_object = get_sub_field('resource_slider_resource_post');

                    if( $post_object ){
                        $post = $post_object;

                        setup_postdata( $post );
                        get_template_part("templates/posts" . $post->post_type );
                        wp_reset_postdata();
                    }
                    endwhile;
                ?>
                <?php 
                    foreach( $posts as $post) : 
                    
                        setup_postdata( $post ); 
                        get_template_part("templates/posts/" . $post->post_type );
                        wp_reset_postdata();
                    endforeach; 
                ?>
            </ul>
            <ul class="uk-slider-nav uk-dotnav uk-flex-center"></ul>
        </div>

        
    </div>
</section>