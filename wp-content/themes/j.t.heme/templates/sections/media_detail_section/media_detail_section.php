<?php 
    $media_position = get_sub_field( "media_detail_detail_position" ); 

    get_template_part( "templates/sections/media_detail_section/media_detail_" . $media_position );
?>

    