<?php 
    $overlay = get_sub_field("media_detail_overlay_detail"); 
    $detail_style = get_sub_field('media_detail_detail_style');
    
    $media_cols = "2";
    if( gettype( get_sub_field('media_detail_media_columns') ) === "string" ){
        $media_cols = get_sub_field('media_detail_media_columns'); 
    } 
?>

<section style="background-color:<?php the_sub_field("background_color"); ?>" class="media-detail-section media-detail-right uk-child-width-expand@m uk-card uk-grid-collapse uk-position-relative<?php echo $overlay ? ' overlay-detail style-' . $detail_style : ''; ?>" uk-grid>
    <div class="uk-width-1-<?php echo $overlay ? '1' : $media_cols; ?>@m"><?php get_template_part( "templates/sections/media_detail_section/media" ); ?></div>
    <div uk-scrollspy="target:.detail-content>*; cls:uk-animation-slide-right; delay:500" class="detail <?php echo $overlay ? 'uk-width-1-2@m uk-overlay uk-position-center-right jt-z-1' : ''; ?>"><?php get_template_part( "templates/sections/media_detail_section/detail" ); ?></div>
</section>