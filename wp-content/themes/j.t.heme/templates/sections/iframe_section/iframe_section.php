
<?php 
    $iframe_class = "uk-position-absolute";
    $aspect_ratio = "jt-ar-" . get_sub_field("iframe_apsect_ratio");
    $height = "100%";

    if( get_sub_field('iframe_height') ){ 
        $height = get_sub_field("iframe_height") . "px";
        $aspect_ratio = "";
        $iframe_class = "";
    }
?>

<section style="background-color:<?php the_sub_field("background_color"); ?>" class="iframe-section <?php echo get_sub_field("iframe_padding"); ?>">
    <?php if( get_sub_field('iframe_header_content') ) : ?>
        <div class="uk-margin-large-bottom">
            <?php the_sub_field('iframe_header_content'); ?>
            <?php get_template_part( "templates/buttons" ); ?>
        </div>
    <?php endif; ?>
    <div class="<?php echo $aspect_ratio; ?>">
        <iframe class="<?php echo $iframe_class; ?>" src="<?php echo get_sub_field("iframe_url"); ?>" width="100%" height="<?php echo $height; ?>"></iframe>
    </div>
</section>