<section style="background-color:<?php the_sub_field("background_color"); ?>" class="uk-padding">
    <?php the_sub_field('full_text_content'); ?>
    <?php get_template_part( "templates/buttons" ); ?>
</section>