
<?php 
    $item_bg_color = get_sub_field("item_background_color");
    $show_dropshadow = false;

    if( $item_bg_color == "#ffffff" || $item_bg_color == "#fff" || $item_bg_color == "#FFF" || $item_bg_color == "#FFFFFF" ){
        $show_dropshadow = true;
    }

    $row_count = count(get_sub_field('mission_detail_grid_Item')); 
    $invert_item_content = get_sub_field('invert_item_content');
    the_sub_field("mission_detail_grid_item_background_color");
?>

<section class="mission-detail-grid-section uk-padding" >
    <div style="z-index:-1" class="background-container uk-position-top uk-position-absolute uk-height-1-1 uk-width-1-1">
        <div style="background-color: <?php echo get_sub_field("background_color"); ?>" class="uk-height-1-1 uk-width-1-1"></div>
    </div>
    <div class="section-heading" uk-scrollspy="target: > *; cls:uk-animation-fade; delay:100;">
        <?php if( get_sub_field('mission_detail_grid_heading_content') ) : ?>
            <?php the_sub_field('mission_detail_grid_heading_content'); ?>
        <?php endif; ?>
        <?php get_template_part( "templates/buttons" ); ?>
    </div>
    <div class="grid-container uk-padding-large uk-padding-remove-right uk-padding-remove-vertical uk-margin-large-top">
        <div uk-scrollspy="target: > div > div > div; cls:uk-animation-slide-bottom; delay: 200" class="uk-child-width-1-2@s uk-child-width-expand@m uk-grid-small uk-grid-match" uk-grid>
            <?php while( have_rows('mission_detail_grid_Item') ): the_row(); $row_index = get_row_index(); ?>
            <div>
                    <?php 
                        $border = '' ; if($row_index === 1){ $border = 'jt-border-tl-30'; }
                        elseif($row_index === $row_count){ $border = 'jt-border-br-30'; }
                    ?>
                    <div class="uk-overflow-hidden <?php echo $show_dropshadow == true ? 'uk-box-shadow-medium' : ''; ?> uk-padding <?php echo $border; ?>" style="background-color: <?php echo $item_bg_color; ?>">
                        <div>
                            <?php $image = get_sub_field("mission_detail_grid_item_icon"); ?>
                            <?php if(!empty($image)) : ?>
                            <p><img src="<?php echo $image['url']; ?>" height="80" /></p>
                            <?php endif; ?>
                            <div class="<?php echo $invert_item_content ? 'uk-light' : ''; ?>">
                                    <?php the_sub_field("mission_detail_grid_item_content"); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
</section>