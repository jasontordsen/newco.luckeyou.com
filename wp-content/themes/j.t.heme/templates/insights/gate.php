
<?php while ( have_posts() ) : the_post(); ?>

        <div class="uk-container uk-padding uk-margin-large-top">
            <?php while( have_rows("insight_options_gate_fields") ) : the_row(); ?>
                <div class="uk-grid uk-grid-collapse uk-child-width-expand@s">
                    <div class="uk-padding uk-padding-remove-left uk-padding-remove-top">
                        <!-- ICON -->
                        <?php if( get_sub_field('insight_options_gate_icon') ): $icon = get_sub_field('insight_options_gate_icon'); ?>
                            <p><img src="<?php echo $icon['url']; ?>" height="80" /></p>
                        <?php endif; ?>

                        <!-- CONTENT -->
                        <div>
                            <h1><?php the_title(); ?></h1>

                            <?php if(get_sub_field('insight_options_gate_subtitle')) : ?>
                                <h3><?php the_sub_field('insight_options_gate_subtitle'); ?></h3>
                            <?php endif; ?>

                            <?php the_content(); ?>
                        </div>

                        <!-- FEATURES -->
                        <?php if( have_rows('insight_options_gate_features') ) : ?>
                            <ul class="uk-list uk-padding-small uk-padding-remove-horizontal">
                                <?php while( have_rows('insight_options_gate_features') ) : the_row(); ?>
                                <li><span uk-icon="check" class="uk-text-primary uk-margin-small-right"></span><strong><?php the_sub_field('insight_options_gate_feature_title'); ?></strong></li>
                                <?php endwhile; ?>
                            </ul>
                        <?php endif; ?>
                    </div>
                    <?php if( get_sub_field('insight_options_gate_image') ) : ?>
                        <div class="uk-width-1-5@s uk-padding-small">
                            <?php $image = get_sub_field("insight_options_gate_image"); ?>
                            <img data-src="<?php echo $image['url']; ?>" width="100%" uk-img />
                        </div>
                    <?php endif; ?>
                    <div class="uk-width-1-4@s" style="border-radius:10px; padding-bottom:10px; background-color:<?php echo the_sub_field('insight_options_gate_form_background_color'); ?>">
                        <?php if( get_sub_field('insight_options_gate_form_header') ) : ?>
                            <div style="border-radius:10px 10px 0 0; padding:22px 22px 10px 22px; background-color:#393636;"><?php the_sub_field('insight_options_gate_form_header'); ?></div>
                        <?php endif; ?>
                    
                        <?php the_sub_field("insight_options_gate_form"); ?>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>

<?php endwhile; ?>
