<?php if($post->post_parent): ?>
    <?php 
        $children = 
        get_pages(array(
            'child_of' => $post->post_parent,
            'sort_column' => 'menu_order'
        ));
    ?>

    <?php if( count($children) <= 4 ) : ?>
        <div id="page-subnav" class="uk-visible@m uk-background-primary">
            <nav class="uk-navbar-container uk-navbar-transparent" uk-navbar>
                <div class="uk-navbar-center">
                    <ul class="uk-navbar-nav">
                        <?php foreach($children as $item) : ?>
                        <li class="<?php echo $post->ID == $item->ID ? 'uk-active' : ''; ?>"><a href="<?php echo get_permalink( $item->ID ); ?>"><?php echo $item->post_title; ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </nav>
        </div>
    <?php endif; ?>

    
    <div id="page-subnav" class="<?php echo count($children) <= 4 ? 'uk-hidden@m' : '' ?> uk-background-primary">
        <nav class="uk-navbar-container uk-navbar-transparent" uk-navbar="align:center; mode:click;">
            <div class="uk-navbar-center">
                <ul class="uk-navbar-nav">
                    <li>
                        <a href="#"><?php the_title(); ?>&nbsp;<span uk-icon="chevron-down"></span></a>
                        <div class="uk-padding-remove-top uk-navbar-dropdown uk-background-primary" style="position: relative;">
                            <ul class="uk-nav uk-navbar-dropdown-nav uk-nav-center">
                                <?php foreach($children as $item) : if( $post->ID != $item->ID ) : ?>
                                <li><a href="<?php echo get_permalink( $item->ID ); ?>"><?php echo $item->post_title; ?></a></li>
                                <?php endif; endforeach; ?>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
<?php endif; ?>