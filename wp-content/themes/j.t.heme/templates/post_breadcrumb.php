<?php 
    $posts_page = get_field( "post_pages_" . $post->post_type, 'option' );

    $bg_opacity = .5;
    if( get_field("insight_options_gated") || get_field('insight_options_hide_post_header') ){
        $bg_opacity = .7;
    }

    $referer = wp_get_referer();
    
    if($referer){    
        $post_id = url_to_postid( $referer );
        $ref_post_type = get_post_type( $post_id );

        if($ref_post_type !== 'page'){
            unset($post_id);
        }
    }
        
    if( !isset( $post_id ) && $posts_page ){
        $post_id = $posts_page->ID;
    }
?>

<?php if( isset($post_id) ) : ?>
<div id="breadcrumb-nav" class="uk-padding-remove-left uk-position-absolute jt-z-1" style="background-color: rgba(0,0,0,<?php echo $bg_opacity; ?>);">
    <ul class="uk-breadcrumb uk-light uk-padding-remove uk-margin-remove">
        <li class="uk-margin-left">
            <a href="<?php echo get_the_permalink($post_id); ?>" >
                <span uk-icon="chevron-left"></span><?php echo get_the_title($post_id); ?>
            </a>
        </li>
    </ul>
</div>
<?php endif; ?> 