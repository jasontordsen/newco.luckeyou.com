<div>
    <?php while( have_rows('section') ): the_row(); ?>
    <?php $layout = get_row_layout(); ?>
    <?php get_template_part( "templates/sections/". $layout. "/" . $layout ); ?>
	<?php endwhile; ?>
</div>