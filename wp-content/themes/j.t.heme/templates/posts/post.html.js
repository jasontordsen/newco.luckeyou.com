<li class="post post-post uk-margin-bottom">
    <a style="height:100%" class="uk-display-block uk-card uk-card-default" href="{{permalink}}">
        <div class="border"></div>
        <div class="hover-bg"></div>
        {{#if thumbnail}}
            <div class="uk-card-media-top">
                <div class="uk-width-1-1@s uk-background-cover">
                    <div class="uk-background-cover jt-ar-5625" data-src="{{thumbnail}}" uk-img></div>
                </div>
            </div>
        {{/if}}
        <div class="uk-card-body">
            {{date}}
            <h3>{{title}}</h3>
            {{excerpt}}<span class="readmore"> Read more</span>
        </div>
    </a>
</li>