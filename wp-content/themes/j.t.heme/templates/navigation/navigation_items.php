<?php if( get_row_layout() == 'navigation_item_menu' ): ?>

    <!-- MENU LAYOUT -->
    <?php while( have_rows('navigation_item_menu_fields') ): the_row(); ?>

        <?php if( get_sub_field('navigation_item_menu_header') ) :?>

            <?php get_template_part("templates/navigation/menu_item/menu_item_w_header"); ?>

        <?php else: ?>

            <?php get_template_part("templates/navigation/menu_item/menu_item_no_header"); ?>

        <?php endif; ?>
        
    <?php endwhile; ?>
    
<?php elseif( get_row_layout() == 'navigation_item_link' ): ?>

    <!-- LINK LAYOUT -->
    <?php $link = get_sub_field('navigation_item_link_link'); ?>
    <li class="uline-nav-btn"><a href="<?php echo $link['url']; ?>"><h6><b><?php echo $link['title']; ?></b></h6></a></li>

<?php endif; ?>