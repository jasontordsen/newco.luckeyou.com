<?php 
    $link = get_sub_field('navigation_item_menu_link'); 
?>

<li class="uline-nav-btn">
    <a href="#"><h6><b><?php the_sub_field('navigation_item_menu_label'); ?></b></h6></a>
    <div class="uk-navbar-dropdown uk-padding jt-background-light">
        <div class="uk-child-width-expand@m" uk-grid>
                    
            <?php get_template_part("templates/navigation/menu_item/items"); ?>

            <?php get_template_part("templates/navigation/menu_item/media"); ?>
        </div>
    </div>
</li>