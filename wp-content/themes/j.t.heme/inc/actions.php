<?php 
 
	function register_menus()
	{
	    register_nav_menus(array( // Using array to specify more menus if needed
	        'main-menu' => __('Main Menu', 'groundtruth'), // Main Navigation
	        'footer-menu' => __('Footer Menu', 'groundtruth'), // Footer Navigation
	        'social-menu' => __('Social Menu', 'groundtruth') // Social Navigation
	    ));
	}

	// function remove_head_scripts() {
	// 	remove_action('wp_head', 'wp_print_scripts');
	// 	remove_action('wp_head', 'wp_print_head_scripts', 9);
	// 	remove_action('wp_head', 'wp_enqueue_scripts', 1);
		
	// 	add_action('wp_footer', 'wp_print_scripts', 5);
	// 	add_action('wp_footer', 'wp_enqueue_scripts', 5);
	// 	add_action('wp_footer', 'wp_print_head_scripts', 5);
	// }

	// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
	function init_pagination()
	{
	    global $wp_query;
	    $big = 999999999;
	    echo paginate_links(array(
	        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
	        'format' => '?paged=%#%',
	        'current' => max(1, get_query_var('paged')),
	        'total' => $wp_query->max_num_pages
	    ));
	}

	// Load HTML5 Blank scripts (header.php)
	function init_header_scripts()
	{
	    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {
			//ajax posts
			wp_enqueue_script('handlebars',  get_template_directory_uri() . '/js/vendor/handlebars.min-latest.js', array(), '1.0', true );
			
			wp_register_script( 'ajax-posts', get_template_directory_uri() . '/js/ajax-posts.js', array(), '1.0', true );
			wp_localize_script( 'ajax-posts', 'ajax_posts', array(
				'ajax_url' => admin_url( 'admin-ajax.php' ),
				'posts_directory' => get_template_directory_uri() . '/templates/posts/',
				'partials_directory' => get_template_directory_uri() . '/templates/partials/'
			));
			wp_enqueue_script('ajax-posts'); // Enqueue it!

			//uikit
			wp_enqueue_script('uikit', get_template_directory_uri() . '/js/vendor/uikit.js', array(), '1.0.0');
			wp_enqueue_script('uikit-icons', get_template_directory_uri() . '/js/vendor/uikit-icons.min.js', array(), '1.0.0' );
			wp_enqueue_script('jquery', get_template_directory_uri() . '/js/vendor/jquery-3.3.1.min.js', array(), '3.3.1');

			wp_enqueue_script('main', get_template_directory_uri() . '/js/main.js', array(), '1.0.0', true );
	    }
	}

	// Load HTML5 Blank styles
	function init_styles()
	{	
		wp_enqueue_style('main', get_template_directory_uri() . '/css/main.min.css', array(), '1.0', 'all');
	}
?>