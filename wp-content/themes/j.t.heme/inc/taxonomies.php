<?php 

	/*------------------------------------*\
	    Custom Taxonomies
	\*------------------------------------*/

	function gt_register_taxonomy( $_slug, $_singular, $_plural, $_post_types, $_order )
	{
	    $labels = array(
	        'name'              => _x( $_plural, 'taxonomy general name' ),
	        'singular_name'     => _x( $_singular, 'taxonomy singular name' ),
	        'search_items'      => __( "Search $_plural" ),
	        'all_items'         => __( "All $_plural" ),
	        'parent_item'       => __( "Parent $_singular" ),
	        'parent_item_colon' => __( "Parent $_singular:" ),
	        'edit_item'         => __( "Edit $_singular" ),
	        'update_item'       => __( "Update $_singular" ),
	        'add_new_item'      => __( "Add New $_singular" ),
	        'new_item_name'     => __( "New $_singular Name" ),
	        'menu_name'         => __( $_plural ),
	    );

	    $args = array(
	        'hierarchical'      => true,
	        'labels'            => $labels,
	        'show_ui'           => true,
	        'show_admin_column' => true,
	        'query_var'         => true,
	        'rewrite'           => array( 'slug' => $_slug ),
			'tax_order'			=>$_order,
			'show_in_rest' 		=> true
	    );	

	    register_taxonomy( $_slug, $_post_types , $args );
	}

	function init_register_taxonomies(){
		gt_register_taxonomy( 'solution', 'Solution', 'Solutions', array('post','insight','event'), 2  );
		gt_register_taxonomy( 'insight_type', 'Insight Type', 'Insight Types', array('insight'), 3  );
		gt_register_taxonomy( 'event_type', 'Event Type', 'Event Types', array('event'), 3  );
	}
?>