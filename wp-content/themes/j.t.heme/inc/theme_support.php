<?php 

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    // add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');

    add_image_size('full', 1920, '', true); // Large Thumbnail
    add_image_size('large', 1200, '', true); // Large Thumbnail
    add_image_size('medium_large', 768, '', true); // Medium Thumbnail
    add_image_size('medium', 414, '', true); // Medium Thumbnail
    add_image_size('small', 320, '', true); // Medium Thumbnail
    add_image_size('square', 480, 480, true); // Custom Thumbnail Size call using 
    add_image_size('thumbnail', 320, 320, true); // Small Thumbnail
}

?>