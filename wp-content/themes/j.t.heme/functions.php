<?php 
	if (!session_id()) {
	    session_start();
	}

	global $num_posts_per_page;
	$num_posts_per_page = 12;

	require_once('inc/theme_support.php');
	require_once('inc/post_types.php');
	require_once('inc/taxonomies.php'); 
	require_once('inc/actions.php');
	require_once('inc/filters.php');
	require_once('inc/menus.php');
	require_once('inc/acf_field_groups.php');
	require_once('inc/ajax_posts.php');

	add_action( 'init', 'init_register_post_types', 0 );
	add_action( 'init', 'init_register_taxonomies', 0 );
	add_action( 'wp_enqueue_scripts', 'init_header_scripts' ); // Add Custom Scripts to wp_head
	add_action( 'init', 'init_pagination' ); // Add our HTML5 Pagination
	add_action( 'init', 'register_menus' ); // Add blank Menus
	add_action( 'wp_enqueue_scripts', 'init_styles' ); // Add Theme Stylesheet

	add_action( 'wp_ajax_nopriv_ajax_posts_get_posts_by_post_type', 'ajax_posts_get_posts_by_post_type' );
	add_action( 'wp_ajax_ajax_posts_get_posts_by_post_type', 'ajax_posts_get_posts_by_post_type' );

	add_action( 'wp_ajax_nopriv_ajax_posts_init_first_page', 'ajax_posts_init_first_page' );
	add_action( 'wp_ajax_ajax_posts_init_first_page', 'ajax_posts_init_first_page' );

	
	add_filter( 'body_class', 'add_slug_to_body_class' ); // Add slug to body class (Starkers build)
	add_filter( 'wp_nav_menu_args', 'remove_nav_div' ); // Remove surrounding <div> from WP Navigation
	add_filter( 'the_category', 'remove_category_rel_from_category_list' ); // Remove invalid rel attribute
	add_filter( 'show_admin_bar', 'remove_admin_bar' ); // Remove Admin bar
	add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10 ); // Remove width and height dynamic attributes to thumbnails
	add_filter( 'image_send_to_editor', 'remove_thumbnail_dimensions', 10 ); // Remove width and height dynamic attributes to post images

	remove_filter( 'the_excerpt', 'wpautop' );
	add_filter('get_the_excerpt', 'custom_short_excerpt');
	add_filter('excerpt_more', 'new_excerpt_more');

	
	add_filter('upload_mimes', 'cc_mime_types');
	
	// add_action( 'wp_enqueue_scripts', 'remove_head_scripts' );

	add_filter( 'wp_default_scripts', 'change_default_jquery' );

	function change_default_jquery($scripts){
		if(!is_admin()){
			$scripts->remove( 'jquery');
			// $scripts->add( 'jquery', false, array( 'jquery-core' ), '1.10.2' );
		}
	}
?>