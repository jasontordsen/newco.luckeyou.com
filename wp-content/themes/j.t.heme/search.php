<?php get_header(); ?>

<div class="uk-padding uk-background-secondary uk-light uk-margin-bottom">
    <div class="uk-margin-large-top">
        <h1 class="uk-width-3-4"><strong>Search results for "<?php the_search_query(); ?>"</strong></h1>
    </div>
</div>

<section class="resource-grid-section uk-padding uk-padding-remove-horizontal" >
    <ul id="posts-container" uk-scrollspy="target: > li; cls:uk-animation-scale-up; delay: 100" class="uk-grid uk-grid-small uk-child-width-1-2@s uk-child-width-1-3@m uk-padding" uk-grid>
        <?php while ( have_posts() ) : the_post(); ?>
        <?php get_template_part( "templates/posts/" . $post->post_type ); ?>
<?php endwhile; ?>
    </ul>
</section>

<?php get_footer(); ?>