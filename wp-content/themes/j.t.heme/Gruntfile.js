module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concat: {
      dist:{
        src: [
        ],
        dest: 'build/js/<%= pkg.buildName %>.pkg.js'
      }
    },
    uglify: {
      all: {
        options: {
        },
        files: [
        ]
      }
    },
    sass: {
      dist:{
        options: {                        
          style: 'compressed'
        },
        files: {                        
          'css/<%= pkg.buildName %>.min.css' : 'src/scss/<%= pkg.buildName %>.scss'
        }
      }
    },
    watch: {
      sass: {
        files: ['src/scss/**/*.scss'],
        tasks: ['sass']
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');

  grunt.registerTask('default', ['uglify','sass']);
};