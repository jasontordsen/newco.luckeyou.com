			<div class="footer uk-light">
				<div class="uk-padding uk-background-secondary uk-grid">
					<div class="uk-width-1-2@s">
						<nav class="uk-navbar-container uk-margin uk-navbar-transparent" uk-navbar>
							<ul class="uk-iconnav">
								<?php while( have_rows('social_nav', 'option') ): the_row(); ?>
								<li><a href="<?php the_sub_field('url'); ?>"><span class="uk-icon-button" uk-icon="<?php the_sub_field('icon'); ?>"></span></a></li>
								<?php endwhile; ?>
							</ul>
						</nav>
						<p>
							<?php while( have_rows('footer_links', 'option') ): the_row(); ?>
								<?php $link = get_sub_field('link'); ?>
								<a href="<?php echo $link['url']; ?>" class="uk-link-text"><?php echo $link['title']; ?></a>
							<?php endwhile; ?>
						</p>
					</div>
				</div>
			</div>
		</div>

		<div id="modal-media-video" class="uk-flex-top uk-modal" uk-modal>
			<div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">

				<button class="uk-modal-close-default uk-button-primary" type="button" uk-close></button>

				<div class="jt-ar-5625">
					<iframe style="top:0; left:0;" class="uk-position-absolute" width="100%" height="100%" frameborder="0" uk-video></iframe>
				</div>
			</div>
		</div>
		
		<?php wp_footer(); ?>
	</body>
</html>
