<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

define('FS_METHOD', 'direct');

/** The name of the database for WordPress */
define('DB_NAME', $_SERVER["RDS_DB_NAME"]);
/** MySQL database username */
define('DB_USER', $_SERVER["RDS_USERNAME"]);
/** MySQL database password */
define('DB_PASSWORD', $_SERVER["RDS_PASSWORD"]);
/** MySQL hostname */
define('DB_HOST', $_SERVER["RDS_HOSTNAME"]);
/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');
/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define( 'AS3CF_SETTINGS', serialize( array(
    'provider' => 'aws',
    'access-key-id' => 'AKIAW6BGZMGH23WGKYHS',
    'secret-access-key' => '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',
) ) );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Y{!)5Kx<0HuuKGr)}h/P+1C^r*?8usAi6I_i%C{Z#@5>M`EJnDRTvIpmvlYPTf=#');
define('SECURE_AUTH_KEY',  '4e?lI.JNB*zx?ZL*;v5t&q8UZwLu!%[$};_T[<*<`M?r$nz})(G:<dC9W,ysbn8M');
define('LOGGED_IN_KEY',    'KMnN)s6aOObQ ~Q11uJeX#%%K2FB-nqU<cgvo 9IMPZuv.ZtqJM9pT/KE=*x$W 6');
define('NONCE_KEY',        '8a2H*R-U~[oj*DP1*/@O0KHA-n^=@n}jsS8io^+-]8i{kE&fboBmtSDWsR|fne*z');
define('AUTH_SALT',        'lxiBSy`SmlJ{X;()4C8dE s]:zxNH~02A/b4]^J?aY!TLUHHR7%C&kxgi^@U#f-Z');
define('SECURE_AUTH_SALT', '/5>ovWA9wz,O= iJP8.wFBb&-=D<hUj.7Xy,.d6IgSxjA6-^D[^`GX4Cvt7yc>gb');
define('LOGGED_IN_SALT',   'Hp<=j=Q-lei 4uA9$S1WwN)R$o#/G1Ii[kIjLP$]h7PZHHG:m]c=I5O9pb#XqoP3');
define('NONCE_SALT',       'R@mUWoq)C/fV6+:.]L51oAN?Yj}~9ih#}#0`QBb8W_cBm,USg:UN8~1G1vNL)i+/');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
